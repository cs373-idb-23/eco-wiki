import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home from "./components/Home";
import About from "./components/About";
import News from "./components/News";
import Policies from "./components/Policies";
import Community from "./components/Communities";
import PolicyInstance from "./components/policy/Policy.jsx"
import NewsInstance from "./components/newsInstances/NewsInstance"
import CommunityInstance from "./components/community/Community.jsx";
import Visualizations from "./components/Visualizations";

const RouteSwitch = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/news" element={<News />} />
        <Route path="/Policies" element={<Policies />} />
        <Route path="/community" element={<Community />} />
        <Route path="/visualizations" element={<Visualizations />} />
        <Route path="/policies/:id" element={<PolicyInstance />} />
        <Route path="/news/:id" element={<NewsInstance />} />
        <Route path="/communities/:id" element={<CommunityInstance />} />
      </Routes>
    </BrowserRouter>
  );
};

export default RouteSwitch;
