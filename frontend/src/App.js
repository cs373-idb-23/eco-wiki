import React from "react";
import RouteSwitch from "./RouteSwitch";
import "./App.css";
import NavMenu from "./components/NavMenu";

function App() {
  return (
    <div className="App">
      <NavMenu />
      <RouteSwitch />
    </div>
  );
}

export default App;
