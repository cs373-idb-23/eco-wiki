import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, ResponsiveContainer } from 'recharts';

const data = [
    {region: 'Western Pacific', count: 4043}, 
    {region: 'Europe', count: 8028}, 
    {region: 'Americas', count: 5966}, 
    {region: 'Africa', count: 5825}, 
    {region: 'Eastern Mediterranean', count: 3100}, 
    {region: 'South-East Asia', count: 1793}, 
    {region: 'Global', count: 184}, 
    {region: 'N/A', count: 642}]

function IllnessesPerRegion() {
    return (
        <Container fluid="md" className='mb-5'>
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center" style={{ color:"#185E3F" }}>Number of Illnesses for each Region</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <BarChart
                            width={500}
                            height={300}
                            data={data}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="region" stroke="#185E3F" />
                            <YAxis stroke="#185E3F" />
                            <ReferenceLine y={52919.29} stroke="#185E3F" />
                            <Tooltip />
                            <Legend />
                            <Bar dataKey="count" fill="#185E3F" />
                        </BarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );
}

export default IllnessesPerRegion