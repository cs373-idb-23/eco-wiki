import React from "react";
import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { PieChart, Pie, Tooltip, ResponsiveContainer } from 'recharts';
// import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, ResponsiveContainer } from 'recharts';

const data = [
    {
        route: "MULTI",
        count: 13
    }, 
    {
        route: "ORAL",
        count: 647
    }, 
    {
        route: "INJECTION",
        count: 156
    }, 
    {
        route: "RECTAL",
        count: 3
    }, 
    {
        route: "INTRATRACHEAL",
        count: 1
    }, 
    {
        route: "ENTERAL",
        count: 1
    }, 
    {
        route: "INTRAVENOUS",
        count: 33
    }, 
    {
        route: "TOPICAL",
        count: 55
    }, 
    {
        route: "OPHTHALMIC",
        count: 22
    }, 
    {
        route: "DENTAL",
        count: 3
    }, 
    {
        route: "INTRAMUSCULAR",
        count: 5
    }, 
    {
        route: "TRANSDERMAL",
        count: 6
    }, 
    {
        route: "VAGINAL",
        count: 8
    }, 
    {
        route: "SUBCUTANEOUS",
        count: 8
    }, 
    {
        route: "SINGLE-DOSE",
        count: 2
    }, 
    {
        route: "INHALATION",
        count: 16
    }, 
    {
        route: "OTIC",
        count: 2
    }, 
    {
        route: "IV (INFUSION)",
        count: 2
    }, 
    {
        route: "INTRAPERITONEAL",
        count: 2
    }, 
    {
        route: "INTRATHECAL",
        count: 1
    }, 
    {
        route: "NASAL",
        count: 2
    }, 
    {
        route: "IRRIGATION",
        count: 2
    }, 
    {
        route: "EPIDURAL",
        count: 1
    }, 
    {
        route: "SUBLINGUAL",
        count: 1
    }
]
/* Bar Chart */
// function DrugsPerRoute() {
//     return (
//         <Container fluid="md" className='mb-5'>
//             <Row style={{ width: "100%", height: 600 }}>
//                 <h3 className="p-5 text-center" style={{ color:"#185E3F" }}>Drugs Count for each Route</h3>
//                 <Col>
//                     <ResponsiveContainer width="100%" height="100%">
//                         <BarChart
//                             width={500}
//                             height={300}
//                             data={data}
//                             margin={{
//                                 top: 5,
//                                 right: 30,
//                                 left: 20,
//                                 bottom: 5,
//                             }}
//                         >
//                             <CartesianGrid strokeDasharray="3 3" />
//                             <XAxis dataKey="route" stroke="#185E3F" />
//                             <YAxis stroke="#185E3F" />
//                             <ReferenceLine y={52919.29} stroke="#185E3F" />
//                             <Tooltip />
//                             <Legend />
//                             <Bar dataKey="count" fill="#185E3F" />
//                         </BarChart>
//                     </ResponsiveContainer>
//                 </Col>
//             </Row>
//         </Container>
//     );
// }

/* Pie Chart */
const DrugsPerRoute = () => {
    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center" style={{ color:"#185E3F" }}>Drugs Count for each Route</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <PieChart width={500} height={500}>
                            <Pie
                                dataKey="count"
                                nameKey="route"
                                isAnimationActive={false}
                                data={data}
                                cx="50%"
                                cy="50%"
                                outerRadius={200}
                                fill="#185E3F"
                                label
                            />
                           <Tooltip/>
                        </PieChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );
}

export default DrugsPerRoute;
