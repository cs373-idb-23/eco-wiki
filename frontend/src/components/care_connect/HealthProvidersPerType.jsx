import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { RadialBarChart, RadialBar, Legend, ResponsiveContainer } from 'recharts';

const data = [
    {name: 'NPI-1', count: 94, fill: '#185E3F'},
    {name: 'NPI-2', count: 116, fill: '#000000'},
];

const style = {
    top: '50%',
    right: 0,
    transform: 'translate(0, -50%)',
    lineHeight: '24px',
  };

const HealthProvidersPerType = () => {
    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center" style={{ color:"#185E3F" }}>Number of Health Providers in Each Type</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <RadialBarChart cx="50%" cy="50%" innerRadius="10%" outerRadius="80%" barSize={50} data={data}>
                            <RadialBar
                                minAngle={15}
                                label={{ position: 'insideStart', fill: '#fff' }}
                                background
                                clockWise
                                dataKey="count"
                            />
                            <Legend iconSize={20} layout="vertical" verticalAlign="middle" wrapperStyle={style} />
                        </RadialBarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );
}

export default HealthProvidersPerType;
