import React, { useEffect, useState } from 'react'
import axios from "axios";

import { Container, Row, Col, Card, Button } from 'react-bootstrap'
import { teamInfo } from "../staticInfo/TeamInfo";
import { ApiInfo } from '../staticInfo/ApiInfo';
import { ToolsInfo } from '../staticInfo/ToolsInfo';

// Backend Stuff for Gitlab API (Personal Access Token Config)
const client = axios.create({
  baseURL: "https://gitlab.com/api/v4/",
  headers: { Authorization: "Bearer glpat-Gu4VxAoTVB5GxhpC317t" },
});

// Fetch GitLab Data
const fetchGitLabData = async () => {
  let totalCommits = 0,
    totalIssues = 0,
    totalUnitTests = 0;

  teamInfo.forEach((member) => {
    member.commits = 0;
    member.issues = 0;
    totalUnitTests += member.unit_tests;
  });

  await client
    .get("projects/43384461/repository/contributors")
    .then((response) => {
      response.data.forEach((element) => {
        const { name, commits } = element;

        teamInfo.forEach((member) => {
          if (member.name === name || member.id === name) {
            member.commits = commits;
          }
        });
        totalCommits += commits;
      });
    });
  
  await client.get("projects/43384461/issues").then((response) => {
    response.data.forEach((element) => {
      const { assignees } = element;
      assignees.forEach((assignee) => {
        const { name, email } = assignee;
        teamInfo.forEach((member) => {
          if (
            member.name === name ||
            member.gitlab_username === name ||
            member.email === email
          )
            member.issues += 1;
        });
      });
      totalIssues += 1;
    });
  });


  console.log("Members");
  teamInfo.forEach((member) => {
    console.log(member.issues);
  });
  
  
  return {
    totalCommits: totalCommits,
    totalIssues: totalIssues,
    totalTests: totalUnitTests,
    teamInfo: teamInfo,
  };
};
  

function About() {
  const [teamList, setTeamList] = useState([]);
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);
  const [totalTests, setTotalTests] = useState(0);

  useEffect(() => {
    fetchGitLabData().then((data) => {
      setTeamList(data.teamInfo);
      setTotalCommits(data.totalCommits);
      setTotalIssues(data.totalIssues);
      setTotalTests(data.totalTests);
    });
  }, [teamList]);


  return (
    <div style={{ backgroundColor: '#abd1c6'}}>
      <Row style={{padding:'2rem 6rem 0rem 2rem'}} className='text-center mx-5'>
        <Col>
          <Container className='p-4'>
            <h2 className='mb-4'>What is EcoWiki?</h2>
            <p>EcoWiki provides up to date information on US policies/bills, current events, and trending Instagram posts about environmental science.</p>
          </Container>
        </Col>
        <Col>
          <Container className='p-4'>
            <h2 className='mb-4'>How does it work?</h2>
            <p>EcoWiki uses the Instagram API to gather tweets about environmental science alongside scrapping information about US policies and bills relating to environmental science as well as current news.</p>
          </Container>
        </Col>
      </Row>
      <Container className='p-4 mb-5'>
        <h2 className='text-center mb-4'>Meet the team</h2>
        <Row>
          {teamList.map((member) => (
            <Col key={member.id} className='py-2'>
              <Card className='card-deck' style={{ width: '14rem' }} border='success'>
                <Card.Img variant="top" src={member.img} className='memberImg' style={{ marginTop: 0}} height='250' />
                <Card.Body style={{ height: '17rem', paddingTop: 0, paddingBottom:'2rem'}}>
                  <Card.Title style={{ color:'#185E3F'}}>{member.name}</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted fst-italic">@{member.id}</Card.Subtitle>
                  <Card.Text><small>{member.bio}</small></Card.Text>
                </Card.Body>
                <Card.Footer className='bg-success fw-white text-white' styles={{ display:'flex!important'}}>
                  <div className="mt-1">Commits: {member.commits}</div>
                  <div className="">Issues: {member.issues}</div>
                  <div className="mb-1">Unit Tests: {member.unit_tests}</div>
                </Card.Footer>
              </Card>
            </Col>))}
          </Row>
        </Container>
        <Container className='pb-5'>
          <h2 className='text-center mb-4'>Overall Stats</h2>
          <Row className='mx-5'>
            <Col>
              <Card className='card-deck' border='success'>
                <Card.Body>
                  <Card.Title style={{ color:'#185E3F'}}>Total Commits</Card.Title>
                  <Card.Text>{totalCommits}</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card className='card-deck' border='success'>
                <Card.Body>
                  <Card.Title style={{ color:'#185E3F'}}>Total Issues</Card.Title>
                  <Card.Text>{totalIssues}</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card className='card-deck' border='success'>
                <Card.Body>
                  <Card.Title style={{ color:'#185E3F'}}>Total Unit Tests</Card.Title>
                  <Card.Text>{totalTests}</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <Row className='mx-5 mt-3 text-center'>
            <Col>
              <Card className='card-deck' border='success'>
                <Card.Body>
                  <Card.Title as="a" href='https://documenter.getpostman.com/view/25799511/2s93CExcGV' style={{ color:'#185E3F', fontSize:'18px', fontWeight:'bold', fontStyle:'italic'}}>
                    API Documentation (Postman)
                  </Card.Title>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card className='card-deck' border='success'>
                <Card.Body>
                  <Card.Title as="a" href='https://gitlab.com/cs373-idb-23/eco-wiki' style={{ color:'#185E3F', fontSize:'18px', fontWeight:'bold', fontStyle:'italic'}}>
                    Repository (Gitlab)
                  </Card.Title>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
        <Container className='mb-5'>
          <h2 className='text-center mb-4'>Tools Used</h2>
          <Row className='mx-5 mt-3 text-center'>
          {ToolsInfo.map((tool) => (
            <Col key={tool.name}  className="d-flex align-self-stretch">
              <Card className='card-deck mb-4' border='success' style={{ width:'17rem'}}>
                <Card.Body>
                  <img src={tool.img} alt="" className='memberImg' style={{ marginTop: 0, objectFit: 'contain'}} height='220' />
                  <Card.Title className='pt-3'>{tool.name}</Card.Title>
                  <Card.Text>{tool.description}</Card.Text>
                  <Button href={tool.url} className='btn btn-success'>More Info</Button>
                </Card.Body>
              </Card>
            </Col>))}
          </Row>
        </Container>
        <div className='pb-5 pt-5 px-5' style={{ backgroundColor:'#185E3F'}}>
          <h2 className='text-center mb-4 text-white'>API References</h2>
          <Row className='mx-5 mt-3 text-center'>
          {ApiInfo.map((api) => (
            <Col key={api.name} >
              <Card className='card-deck mb-5' border='success'>
                <Card.Body>
                  <Card.Img variant="top" src={api.img} className='memberImg' />
                  <Card.Title className='pt-4'>{api.name}</Card.Title>
                  <Card.Text>{api.description}</Card.Text>
                  <Button href={api.url} className='btn btn-success'>More Info</Button>
                </Card.Body>
              </Card>
            </Col>))}
          </Row>
        </div>
    </div>
  )
}

export default About;
