import React, { useEffect, useState } from 'react'
import Card from './InstanceCard'
import './newsInstance.css'
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { Spinner } from 'react-bootstrap';

const client = axios.create({
    baseURL: "https://api.ecowiki.me/",
});

function NewsInstance () {

    const { id } = useParams();
    const [news, setNews] = useState();
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        const fetchNews = async () => {
        if (news === undefined) {
            await client
            .get(`articles/id=${id}`)
            .then((response) => {
                setNews(response.data);
            })
            .catch((err) => console.log(err));
        }
        setLoaded(true);
        };
        fetchNews();
    }, [news]);

    return (
        <div className='instance-page'>
        { loaded ?
            (<div>
               {<Card
                    title= {news.article.title}
                    author= {news.article.author}
                    date= {new Date(news.article.published).toDateString()}
                    image={ news.article.img_url ? news.article.img_url : 'https://www.sciencenews.org/wp-content/uploads/2022/12/121722_climate-action_feat-1030x580.jpg'}
                    text={news.article.description}
                    category={news.article.category}
                    language={news.article.language}
                    url={news.article.url}
                />}
                <div className='bg-white p-4 rounded my-4'>
                    <h3 className='py-2'>Related Post (<a href={"/communities/" + news.post.id}>More Details</a>):</h3>
                    <ul style={{ listStyle: "none", paddingLeft: "1rem"}}>
                        <li className='p-1'><b>Caption</b>: {news.post.caption}</li>
                        <li className='p-1'><b>Date Posted</b>: {new Date(news.post.date).toDateString()}</li>
                        <li className='p-1'><b>Likes</b>: {news.post.likes}</li>
                        <li className='p-1'><b>Comments</b>: {news.post.comments}</li>
                        <div className='p-1'><b>Hashtags</b>: {news.post.hashtags.replace(/\s/g, '' ).split(",").map(tag => <span className='rounded' style={{ letterSpacing: "0.07rem", backgroundColor: "lightblue", padding: "0.3rem", margin: "0.1rem", lineHeight: "2rem"}}>{" #" + tag}</span>)}</div>
                        <li className='p-1'><b>Mentions</b>: {news.post.mentions}</li>
                        <li className='p-1'><a href={news.post.url}>Link to the Actual Post on Instagram</a></li>
                    </ul>
                </div> 
            </div>)
            : <Spinner animation="border"/>}
        </div>
    )
}

export default NewsInstance;
