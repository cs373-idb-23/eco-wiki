import React from 'react'
import './newsInstance.css'

function Card({title, author, date, url, text, image, category, language}) {
    return (
        <div className='instance-container'>
            <div className='whole-instance'>
                <div className='bottom-instance'>
                    <div className='instance-content'>
                        <div className='instance-title'>
                            <h3>{title}</h3>
                        </div>
                        <div>
                            <p className='text-center'><a href={url}>View Official Article</a></p>
                        </div>
                        <div className='instance-image d-flex justify-content-center'>
                            <div style={{width:"75%"}}>
                                <img src={image} className='object-fit-contain pb-3'/>
                            </div>
                        </div>
                        <div className='instance-author'>
                            <p><b>Author: </b>{author}</p>
                            <p><b>Date Published: </b>{date}</p>
                        </div>
                        <div className='instance-text'>  
                            <p><b>Description: </b>{text}</p>
                        </div>
                        <div className='instance-text'>  
                            <p><b>Category(s): </b>{category}</p>
                        </div>
                        <div className='instance-text'>  
                            <p><b>Language: </b>{language}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default Card