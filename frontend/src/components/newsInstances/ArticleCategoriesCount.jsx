import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { PieChart, Pie, Tooltip, ResponsiveContainer } from 'recharts';

const data = [
  {
    category: 'politics',
    count: 50,
  },
  {
    category: "programming",
    count: 18,
  },
  {
    category: "health",
    count: 4,
  },
  {
    category: "business",
    count: 18,
  },
  {
    category: "general",
    count: 81,
  },
  {
    category: "politics, world",
    count: 10,
  },
  {
    category: "business, finance",
    count: 21,
  },
  {
    category: "world",
    count: 32,
  },
  {
    category: "economy",
    count: 7,
  },
  {
    category: "finance",
    count: 38,
  },
  {
    category: "technology",
    count: 7,
  },
  {
    category: "regional",
    count: 39,
  },
  {
    category: "academia",
    count: 38,
  },
  {
    category: "national",
    count: 25,
  },
  {
    category: "environment, forestry",
    count: 7,
  },
  {
    category: "academic, CS, AI",
    count: 3,
  },
  {
    category: "science",
    count: 13,
  },
  {
    category: "regional, brampton",
    count: 5,
  },
  {
    category: "regional, politics",
    count: 3,
  },
  {
    category: "industry, fishery",
    count: 3,
  },
  {
    category: "leadership",
    count: 5,
  },
  {
    category: "environment",
    count: 4,
  },
  {
    category: "lifestyle",
    count: 8,
  },
  {
    category: "regional, tulsa, oklahoma",
    count: 3,
  },
  {
    category: "british-columbia, regional",
    count: 3,
  },
  {
    category: "energy",
    count: 9,
  },
  {
    category: "entertainment",
    count: 4,
  },
  {
    category: "industry, textiles",
    count: 3,
  },
  {
    category: "commodity, natural-gas",
    count: 3,
  },
  {
    category: "health, lifestyle",
    count: 3,
  },
  {
    category: "opinion",
    count: 3,
  },
  {
    category: "regional, harrisburg",
    count: 3,
  },
  {
    category: "transportation, logistic",
    count: 4,
  },
  {
    category: "other",
    count: 28,
  },
];

const ArticleCategoriesCount = () => {
  return (
      <Container fluid="md">
          <Row style={{ width: "100%", height: 600 }}>
              <h3 className="p-5 text-center" style={{ color:"#185E3F" }}>Category Count For Articles</h3>
              <Col>
                  <ResponsiveContainer width="100%" height="100%">
                      <PieChart width={500} height={500}>
                          <Pie
                              dataKey="count"
                              nameKey="category"
                              isAnimationActive={false}
                              data={data}
                              cx="50%"
                              cy="50%"
                              outerRadius={200}
                              fill="#185E3F"
                              label
                          />
                         <Tooltip/>
                      </PieChart>
                  </ResponsiveContainer>
              </Col>
          </Row>
      </Container>
  );
}

export default ArticleCategoriesCount;
