import React, { useEffect, useState, useRef } from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import FilterDropdown from "../components/FilterDropdown";
import Pagination from "react-bootstrap/Pagination";
import Spinner from "react-bootstrap/Spinner";
import axios from "axios";
import { Highlight } from "react-highlight-regex";

const client = axios.create({
  baseURL: "https://api.ecowiki.me/",
});

// 1 -> id(1, 20) = id(20 * 0 + 1, 20 * 1)
// 2 -> id(21, 40)
// 3 -> id(41, 60)
// 4 -> id(61, 80)
// 5 -> id(81, 100)


const community_data = [
  {
    "id": 1,
    "img": "https://www.celebritykind.com/wp-content/uploads/2021/10/Screen-Shot-2021-10-30-at-8.03.55-am.png",
    "user": "@mrbeast",
    "date_posted": "Oct 29, 2021",
    "likes": "2,367,052",
    "followers": "23.4M",
    "hashtags": ""
  }, {
    "id": 2,
    "img": "https://imgs.search.brave.com/SRfC7CFhf5eVDfot2CDa7YydpK3cfpMaq-dSIFpRpB8/rs:fit:1200:675:1/g:ce/aHR0cHM6Ly9zdGF0/aWMuZWxkaWFyaW8u/ZXMvY2xpcC9mOTFh/ZmNkMC0xNWEwLTQ0/MDUtYTU4MS0wYzJh/MTMxMTBiYjVfMTYt/OS1kaXNjb3Zlci1h/c3BlY3QtcmF0aW9f/ZGVmYXVsdF8xMDE2/MzkxLmpwZw",
    "user": "@jeromefosterii",
    "date_posted": "Dec 6, 2022",
    "likes": "17,209",
    "followers": "50.8K",
    "hashtags": "ClimateEmergency, EnvironmentalJustice, climatechange, ClimateStrike"
  }, {
    "id": 3,
    "img": "https://imgs.search.brave.com/rEUHDbIjfOoYQUZDSo5ZFgAXc5047i50TntrUSdz_PQ/rs:fit:847:225:1/g:ce/aHR0cHM6Ly90c2Ux/Lm1tLmJpbmcubmV0/L3RoP2lkPU9JUC5t/WW9Cc1hXaDVsV3pj/S3haLWgxaE5nSGFF/SiZwaWQ9QXBp",
    "user": "@jessicamulroney",
    "date_posted": "Jan 10, 2023",
    "likes": "Hidden",
    "followers": "68.9K",
    "hashtags": "browngirlgreen, environmentaljustice, epa, funding, climatejustice, healthequity, environmentalist, ecofriendly, environnement, environmentallyconscious"
  }
]

var queryRE = null;

function Communities() {
  const searchQuery = useRef("");
  const numLikesQuery = useRef("#")
  const numCommentsQuery = useRef("#")
  // const numHashtagsQuery = useRef("#")
  const numMentionsQuery = useRef("#")
  const hashtagsQuery = useRef("#")

  const [sort, setSort] = useState("sort");
  const [ascending, setAscending] = useState(false);
  const [communities, setCommunities] = useState(community_data);
  const [activePage, setActivePage] = useState(1);
  const [hashtag, setHashtag] = useState("Hashtag");
  const [loaded, setLoaded] = useState(false);

  const handleSortFilter = (value) => {
    setSort(value.toLowerCase().replace(" ", "_"));
  };
  const handleOrderFilter = (value) => {
    setAscending(value === "Ascending");
  };
  const handleHashtagFilter = (value) => {
    setHashtag(value);
  };

  function handleClick(number) {
    setActivePage(number);
    setLoaded(false);
  }

  function highlightText(input) {
    if (queryRE !== null) {
      return <Highlight match={queryRE} text={input} />;
    }
    return input;
  }

  // Filters out posts based on number of mentions and hashtags
  // function quantify(post) {
    
  //   if (numHashtagsQuery.current.value !== "*") {
  //     if (post.hashtags === "") {
  //       if (numHashtagsQuery.current.value === 0) {
  //         return true;
  //       } else {
  //         return false;
  //       }
  //     }
  //     if (post.hashtags.split(",").length !== numHashtagsQuery.current.value) {
  //       return false;
  //     }
  //   }

  //   if (numMentionsQuery.current.value !== "*") {
  //     if (post.mentions === "") {
  //       if (numMentionsQuery.current.value === 0) {
  //         return true;
  //       } else {
  //         return false;
  //       }
  //     }
  //     if (post.mentions.split(",").length !== numMentionsQuery.current.value) {
  //       return false;
  //     }
  //   }
  //   return true;
  // }

  useEffect(() => {
    const fetchJobs = async () => {
      if (!loaded) {
        var query = `posts?`;
        if (searchQuery.current.value !== "") {
          query = `search/posts/${searchQuery.current.value}`;
          queryRE = new RegExp(
            `(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
            "i"
          );
        } else {
          queryRE = null;
          if (sort !== "sort") {
            query += `&sort=${sort}`;
          }
          if (ascending) {
            if (sort !== "sort") {
              query += "&asc";
            } else {
              query += "&desc";
            }
          }
          if (numLikesQuery.current.value !== "") {
            query += `&likes=${numLikesQuery.current.value}`;
          }
          if (numCommentsQuery.current.value !== "") {
            query += `&comments=${numCommentsQuery.current.value}`;
          }
          if (hashtag !== "Hashtag") {
            query += `&hashtags=${hashtagsQuery.current.value}`;
          }
        }

        console.log(query);
        await client
          .get(query)
          .then((response) => {
            setCommunities(response.data);
          })
          .catch((err) => console.log(err));
        setLoaded(true);
      }
    };
    fetchJobs();
  }, [communities, ascending, hashtag, sort, loaded]);

  let numPages = Math.ceil(557 / 20);
  let items = [];

  for (let number = activePage - 2; number <= activePage + 2; number++) {
    if (number > 0 && number <= numPages) {
      items.push(
        <Pagination.Item
          key={number}
          onClick={() => handleClick(number)}
          active={number  ===  activePage}
        >
          {number}
        </Pagination.Item>
      );
    }
  }

  return (
    <div className="container">
      <div className="text-center my-4">
      <h1 className= "title">Community</h1>
       <p>Find out how influencers in your community are making a difference through social media!</p>
       </div>
      <div className='cards-container'>
      <Form
        onSubmit={(event) => {
          event.preventDefault();
          setLoaded(false);
        }}
        className="d-flex pb-2 justify-content-center">
        <Form.Control
          ref={searchQuery}
          style={{ width: "20vw" }}
          type="search"
          placeholder="Search Posts"
          className="me-2"
          aria-label="Search"
        />
        <Button variant="outline-light" onClick={() => setLoaded(false)}>
          Search
        </Button>
      </Form>
      <Form className="filter-form">
        <Row className="mx-auto text-center w-50 my-4">
          <Col>
            <FilterDropdown
              title="Sort"
              items={["Sort", "Likes", "Comments", "Caption", "Hashtag", "Mentions"]}
              onChange={handleSortFilter}
            />
          </Col>
          <Col>
            <FilterDropdown
              title="Order"
              items={["Ascending", "Descending"]}
              onChange={handleOrderFilter}
            />
          </Col>
        </Row>
        <Row>
          <Col className="mt-4 d-flex justify-content-center">
            <Form.Label className="text-white pt-2"><b>Number of Comments: </b></Form.Label>
            <Form.Control
              ref={numCommentsQuery}
              style={{ width: "10vw", marginLeft: "10px"}}
              type="search"
              className="me-2"
              aria-label="Search"
              defaultValue=""
              placeholder="#"
              onChange={numCommentsQuery}
            />
          </Col>
          <Col className="mt-4 d-flex justify-content-center">
            <Form.Label className="text-white pt-2"><b>Number of Likes: </b></Form.Label>
            <Form.Control
              ref={numLikesQuery}
              style={{ width: "10vw", marginLeft: "10px"}}
              type="search"
              className="me-2"
              aria-label="Search"
              defaultValue=""
              placeholder="#"
              onChange={numLikesQuery}
            />
          </Col>
          <Col className="mt-4 d-flex justify-content-center">
            <Form.Label className="text-white pt-2"><b>Number of Mentions: </b></Form.Label>
            <Form.Control
              ref={numMentionsQuery}
              style={{ width: "10vw", marginLeft: "10px"}}
              type="search"
              className="me-2"
              aria-label="Search"
              defaultValue="*"
              onChange={numMentionsQuery}
            />
          </Col>
          <Col className="mt-4 d-flex justify-content-center">
            <Form.Label className="text-white pt-2"><b>Hashtag: </b></Form.Label>
            <Form.Control
              ref={hashtagsQuery}
              style={{ width: "10vw", marginLeft: "10px"}}
              type="search"
              className="me-2"
              aria-label="Search"
              defaultValue=""
              onChange={handleHashtagFilter}
            />
          </Col>
        </Row>
        <Row className="mx-auto text-center mt-4 mb-4">
          <Col>
            <Button
              variant="outline-light"
              onClick={() => setLoaded(false)}
            >
              Submit
            </Button>
          </Col>
        </Row>

      </Form>
      <Pagination className="justify-content-center mb-4">
        {activePage > 3 && (
          <Pagination.Item
            first
            key={1}
            onClick={() => handleClick(1)}
            active={1  ===  activePage}
          >
            1
          </Pagination.Item>
        )}
        {activePage > 4 && <Pagination.Ellipsis />}
        {items}
        {activePage < numPages - 3 && <Pagination.Ellipsis />}
        {activePage < numPages - 2 && (
          <Pagination.Item
            last
            key={numPages}
            onClick={() => handleClick(numPages)}
            active={numPages  ===  activePage}
          >
            {numPages}
          </Pagination.Item>
        )}
      </Pagination>
      <div className="items-center text-center text-white">
        <p><b><i>Showing results {20 * (activePage - 1) + 1} - {20 * activePage} of {communities && communities.data && communities.data.length}</i></b></p>
      </div>
      <Row
        xl={4}
        lg={3}
        md={1}
        sm={1}
        xs={1}
        className="d-flex g-4 p-4 justify-content-center"
      >
        { loaded && communities.data ? communities.data.filter((_, i) => i >= (20 * (activePage - 1)) && i < (20 * activePage)).map((community) => (
          <Col key={community.id} className="d-flex align-self-stretch">
            <Card className="rounded">
              <Card.Body style={{ textAlign: "left" }}>
                <Card.Link href={"communities/" + community.id}>
                  <Card.Img src={community.image_url ? community.image_url : "https://upload.wikimedia.org/wikipedia/commons/9/95/Instagram_logo_2022.svg"}/>
                </Card.Link>
                <p className='text-center'><a href={community.url}>View Post</a></p>
                <ul style={{ listStyle: "none", paddingLeft: "1rem"}}>
                  <li className='text-truncate'><b>Caption</b>: {highlightText(community.caption)}</li>
                  <li><b>Date Posted</b>: {new Date(community.date).toDateString()}</li>
                  <li><b>Likes</b>: {community.likes}</li>
                  <li><b>Comments</b>: {community.comments}</li>
                  <li className='text-truncate'><b>Hashtags</b>: {highlightText(community.hashtags)}</li>
                  <li><b>Mentions</b>: {highlightText(community.mentions)}</li>
                </ul>
              </Card.Body>
            </Card>
          </Col>)) : (
            <Spinner animation="grow" />
          )
        }
        </Row>
        <Pagination className="justify-content-center mb-5">
        {activePage > 3 && (
          <Pagination.Item
            key={1}
            onClick={() => handleClick(1)}
            active={1 === activePage}
            first={true}
          >
            1
          </Pagination.Item>
        )}
        {activePage > 4 && <Pagination.Ellipsis />}
        {items}
        {activePage < numPages - 3 && <Pagination.Ellipsis />}
        {activePage < numPages - 2 && (
          <Pagination.Item
            key={numPages}
            onClick={() => handleClick(numPages)}
            active={numPages === activePage}
            last={true}
          >
            {numPages}
          </Pagination.Item>
        )}
      </Pagination>
      </div>
    </div>
  );
}

export default Communities;
