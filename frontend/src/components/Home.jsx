import React from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import CardGroup from "react-bootstrap/CardGroup";
import Carousel from "react-bootstrap/Carousel";

const Home = () => {
  return (
    <div className="main-home">
      <div>
        <Carousel className="ml-5 p-5">
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={require("../assets/splash-carousel-1.jpg")}
              alt="First slide"
            />
            <Carousel.Caption>
              <h1>Welcome to EcoWiki</h1>
              <h3>
                Join the movement towards a sustainable future - explore, learn
                and act with EcoWiki.
              </h3>
              <a href="/About">
                <Button style={{ backgroundColor: "#287ab8" }}>
                  Learn More
                </Button>
              </a>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={require("../assets/splash-carousel-2.jpg")}
              alt="Second slide"
            />
            <Carousel.Caption style={{ color: "black" }}>
              <h3>
                Join the movement towards a sustainable future - explore, learn
                and act with EcoWiki.
              </h3>
              <a href="/About">
                <Button style={{ backgroundColor: "#287ab8" }}>
                  Learn More
                </Button>
              </a>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={require("../assets/splash-carousel-3.jpg")}
              alt="Third slide"
            />
            <Carousel.Caption style={{ color: "black" }}>
              <h3>
                Join the movement towards a sustainable future - explore, learn
                and act with EcoWiki.
              </h3>
              <a href="/About">
                <Button style={{ backgroundColor: "#287ab8" }}>
                  Learn More
                </Button>
              </a>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </div>
      <div
        className="bg d-flex justify-content-center"
        style={{ margin: "50px" }}
      >
        <CardGroup className="gap-4 w-75">
          <Card className="rounded">
            <Card.Img
              className="rounded"
              variant="top"
              src="https://images.unsplash.com/photo-1507745512299-8bd0e0b3380f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=800&q=60"
            />
            <Card.Body style={{ textAlign: "center" }}>
              <Card.Title>
                <Card.Link href="/news">News</Card.Link>
              </Card.Title>
              <Card.Text>
                Stay up-to-date with the latest environmental news and trends.
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className="rounded">
            <Card.Img
              className="rounded"
              variant="top"
              src="https://images.unsplash.com/photo-1569060368746-4b99609d49d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=800&q=60"
            />
            <Card.Body style={{ textAlign: "center" }}>
              <Card.Title>
                <Card.Link href="/policies">Policies</Card.Link>
              </Card.Title>
              <Card.Text>
                Discover the government policies shaping our planet's future.
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className="rounded">
            <Card.Img
              className="rounded"
              variant="top"
              src="https://images.unsplash.com/photo-1571987937324-e91fd2d7e69a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=800&q=60"
            />
            <Card.Body style={{ textAlign: "center" }}>
              <Card.Title>
                <Card.Link href="/community">Community</Card.Link>
              </Card.Title>
              <Card.Text>
                Join the conversation and connect with other eco-conscious
                individuals.
              </Card.Text>
            </Card.Body>
          </Card>
        </CardGroup>
      </div>
    </div>
  );
};

export default Home;
