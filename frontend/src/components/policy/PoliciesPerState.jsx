import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, ResponsiveContainer } from 'recharts';

const data = [
    {state: 'AK', count: 15},
    {state: 'AL', count: 5},
    {state: 'AR', count: 1},
    {state: 'AS', count: 1},
    {state: 'AZ', count: 13},
    {state: 'CA', count: 128},
    {state: 'CO', count: 29},
    {state: 'CT', count: 17},
    {state: 'DE', count: 11},
    {state: 'FL', count: 22},
    {state: 'GA', count: 6},
    {state: 'GU', count: 1},
    {state: 'HI', count: 9},
    {state: 'IA', count: 5},
    {state: 'ID', count: 4},
    {state: 'IL', count: 22},
    {state: 'IN', count: 12},
    {state: 'KS', count: 10},
    {state: 'KY', count: 7},
    {state: 'LA', count: 9},
    {state: 'MA', count: 28},
    {state: 'MD', count: 18},
    {state: 'ME', count: 15},
    {state: 'MI', count: 26},
    {state: 'MN', count: 19},
    {state: 'MO', count: 4},
    {state: 'MS', count: 1},
    {state: 'MT', count: 4},
    {state: 'NC', count: 2},
    {state: 'ND', count: 15},
    {state: 'NE', count: 8},
    {state: 'NH', count: 7},
    {state: 'NJ', count: 26},
    {state: 'NM', count: 30},
    {state: 'NV', count: 18},
    {state: 'NY', count: 47},
    {state: 'OH', count: 16},
    {state: 'OK', count: 5},
    {state: 'OR', count: 32},
    {state: 'PA', count: 23},
    {state: 'RI', count: 15},
    {state: 'SC', count: 12},
    {state: 'SD', count: 7},
    {state: 'TN', count: 9},
    {state: 'TX', count: 28},
    {state: 'UT', count: 7},
    {state: 'VA', count: 15},
    {state: 'VI', count: 4},
    {state: 'VT', count: 18},
    {state: 'WA', count: 36},
    {state: 'WI', count: 10},
    {state: 'WV', count: 20},
    {state: 'WY', count: 13}
]

function PoliciesPerState() {
    return (
        <Container fluid="md" className='mb-5'>
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center" style={{ color:"#185E3F" }}>Number of Policies (Bills) Enacted in Each State</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <BarChart
                            width={500}
                            height={300}
                            data={data}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="state" stroke="#185E3F" />
                            <YAxis stroke="#185E3F" />
                            <ReferenceLine y={52919.29} stroke="#185E3F" />
                            <Tooltip />
                            <Legend />
                            <Bar dataKey="count" fill="#185E3F" />
                        </BarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );
}
export default PoliciesPerState
