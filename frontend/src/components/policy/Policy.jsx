import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Link } from "react-router-dom";
import { Spinner } from "react-bootstrap";

const client = axios.create({
  baseURL: "https://api.ecowiki.me/",
});


const Policy = () => {
  const { id } = useParams();
  const [policy, setPolicy] = useState();
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const fetchPolicy = async () => {
      if (policy === undefined) {
        await client
          .get(`bills/id=${id}`)
          .then((response) => {
            setPolicy(response.data);
          })
          .catch((err) => console.log(err));
      }
      setLoaded(true);
    };
    fetchPolicy();
  }, [policy]);

  return (
    <div className="container-fluid">
      { loaded ? (
        <div>
          <div className="row">
            <h1 className="col-12 text-center my-3 fw-bold"> {policy.bill.title}</h1>
          </div>
          <div className="row justify-content-center mt-5">
            <div className="col-4">
              <img 
              src = {policy.bill.image_url ? policy.bill.image_url : "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Seal_of_the_United_States_House_of_Representatives.svg/600px-Seal_of_the_United_States_House_of_Representatives.svg.png?20110930013415"} 
              alt=""/>
            </div>
            <div className="col-4">
              <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">About</h5>
                    <ul>
                      <li>Date: {policy.bill.introduced}</li>
                      <li>Description: {policy.bill.description}</li>
                      <li>Sponsor(s): {policy.bill.sponsor}</li>
                      <li>Summary: {policy.bill.summary}</li>
                      <li><a href={policy.bill.url}>Link to the Official Bill</a></li>
                    </ul>
                </div>
            </div>
          </div>
        </div>
        <div className="row justify-content-center">
          <div className="mt-5 col-8 mb-5 text-center">
            <div className="col">
              <h5 className="bg-white d-inline-block p-2 rounded"> Related Community Information (<a href={"/communities/" + policy.post.id}>View Post</a>) </h5>
            </div>
            <div className="card">
              <div className="d-flex flex-column justify-content-center">
                <div className="d-flex mx-5 px-5">
                <img 
                src = {policy.post.image_url ? policy.post.image_url : "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Seal_of_the_United_States_House_of_Representatives.svg/600px-Seal_of_the_United_States_House_of_Representatives.svg.png?20110930013415"} 
                alt=""/>
              </div>
              <p className='text-center'><a className="text-decoration-none"href={policy.post.url}><b>View Actual Instagram Post</b></a></p>
              <ul style={{ listStyle: "none", paddingLeft: "1rem"}}>
                <li className="p-1"><b>Caption</b>: {policy.post.caption}</li>
                <li className="p-1"><b>Date Posted</b>: {new Date(policy.post.date).toDateString()}</li>
                <li className="p-1"><b>Likes</b>: {policy.post.likes}</li>
                <li className="p-1"><b>Comments</b>: {policy.post.comments}</li>
                <li className="p-1"><b>Hashtags</b>: {policy.post.hashtags}</li>
                <li className="p-1"><b>Mentions</b>: {policy.post.mentions ? policy.post.mentions : "None"}</li>
              </ul>
              </div>
            </div>
          </div>
        </div>
      </div>) : (<Spinner animation="border"/>)}
    </div>
  );
}

export default Policy;
