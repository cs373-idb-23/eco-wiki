import React, { useState, useEffect, useRef } from 'react'
import Card from 'react-bootstrap/Card'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import FilterDropdown from "../components/FilterDropdown";
import Pagination from 'react-bootstrap/Pagination'
import Spinner from "react-bootstrap/Spinner";
import axios from "axios";
import { Highlight } from "react-highlight-regex";

const client = axios.create({
  baseURL: "https://api.ecowiki.me/",
});

let news_data = [
  {
    "id": 1,
    "title": "Seeking motivated voters, an environment-focused nonprofit turns to red states",
    "author": "Carlos Perez-Beltran",
    "published": "Feb. 09, 2023",
    "description": "The Environmental Voter Project said they see an opportunity to bridge the partisan gap, noting that many Republican-led states are home to some of the worst climate-fueled impacts — and potentially the most significant opportunities for reform.",
    "img_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/NBC_Peacock_1986.svg/2560px-NBC_Peacock_1986.svg.png",
  }, {
    "id": 2,
    "title": "California’s biggest environmental cleanup leaves lead contamination and frustration",
    "author": "Tony Briscoe, Jessica Garrison, Aida Ylanan",
    "published": "Feb. 10, 2023",
    "description": "California’s largest and most expensive environmental cleanup has failed to properly remove lead pollution from some homes and neighborhoods near a notorious battery recycler in southeast Los Angeles County, leaving residents at continued risk, a Times investigation shows.",
    "img_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Los_Angeles_Times_logo.svg/1595px-Los_Angeles_Times_logo.svg.png?20110603161525",
  }, {
    "id": 3,
    "title": "Record Low Sea Ice Cover in the Antarctic",
    "author": "Alfred Wegener Institute",
    "published": "Feb. 10, 2023",
    "description": "There is currently less sea ice in the Antarctic than at any time in the forty years since the beginning of satellite observation: in early February 2023, only 2.20 million square kilometres of the Southern Ocean were covered with sea ice.",
    "img_url": "https://1408440223.rsc.cdn77.org/images/stories/joomlart/enn_logo2.JPG",
  }
]

var queryRE = null;

function News() {
  const searchQuery = useRef("");
  const datePublishedQuery = useRef("YYYY-MM-DD")
  const categoryQuery = useRef("#")

  const [news, setNews] = useState(news_data);
  const [activePage, setActivePage] = useState(1);
  const [loaded, setLoaded] = useState(false);

  const [sort, setSort] = useState("sort");
  const [ascending, setAscending] = useState(false);
  const [language, setLanguage] = useState("Language");
  const [category, setCategory] = useState("Category");
  const [published, setPublished] = useState("Published");
  
  const handleSortFilter = (value) => {
    setSort(value.toLowerCase().replace(" ", "_"));
  };
  const handleOrderFilter = (value) => {
    setAscending(value === "Ascending");
  };
  const handleLanguageFilter = (value) => {
    setLanguage(value);
  };
  const handleCategoryFilter = (value) => {
    setCategory(value);
  };
  const handlePublishedFilter = (value) => {
    setPublished(value);
  };

  function handleClick(number) {
    setActivePage(number);
    setLoaded(false);
  }

  function highlightText(input) {
    if (queryRE != null) {
      return <Highlight match={queryRE} text={input} />;
    }
    return input;
  }

  useEffect(() => {
    const fetchArticles = async () => {
      if (!loaded) {
        var query = `articles`;
        if (searchQuery.current.value !== "") {
          query = `search/articles/${searchQuery.current.value}`;
          queryRE = new RegExp(
            `(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
            "i"
          );
        } else {
          queryRE = null;
          query += '?'
          if (sort !== "sort") {
            query += `&sort=${sort}`;
          }
          if (ascending && sort !== "sort") {
            query += "&asc";
          }
          if (language !== "Language") {
            query += `&language=${language}`;
          }
          if (category !== "Category") {
            query += `&category=${categoryQuery.current.value}`;
          }
          if (published !== "Published") {
            query += `&published=${datePublishedQuery.current.value}`
          }
        }

        console.log(query);
        await client
          .get(query)
          .then((response) => {
            setNews(response.data);
          })
          .catch((err) => console.log(err));
        setLoaded(true);
      }
    };
    fetchArticles();
  }, [news, ascending, category, language, published, sort, loaded]);

  let numPages = 600 / 20;
  let items = [];

  for (let number = activePage - 2; number <= activePage + 2; number++) {
    if (number > 0 && number <= numPages) {
      items.push(
        <Pagination.Item
          key={number}
          onClick={() => handleClick(number)}
          active={number === activePage}
        >
          {number}
        </Pagination.Item>
      );
    }
  }

  return (
    <div className="container">
      <div className="text-center my-4">
        <h1 className= "title">News</h1>
          <p>Discover the latest news regarding the environment.</p>
      </div>
      <Form
        onSubmit={(event) => {
          event.preventDefault();
          setLoaded(false);
        }}
        className="d-flex pb-2 justify-content-center">
        <Form.Control
          ref={searchQuery}
          style={{ width: "20vw" }}
          type="search"
          placeholder="Search Articles"
          className="me-2"
          aria-label="Search"
        />
        <Button variant="outline-light" onClick={() => setLoaded(false)}>
          Search
        </Button>
      </Form>
      <Form className="filter-form">
        <Row className="mx-auto text-center w-50 my-4">
          <Col>
            <FilterDropdown
              title="Sort"
              items={["Sort", "Language", "Author", "Category", "Published"]}
              onChange={handleSortFilter}
            />
          </Col>
          <Col>
            <FilterDropdown
              title="Order"
              items={["Ascending", "Descending"]}
              onChange={handleOrderFilter}
            />
          </Col>
          <Col>
            <FilterDropdown
              title="Language"
              items={["Language", "en", "ja"]}
              onChange={handleLanguageFilter}
            />
          </Col>
        </Row>
        <Row>
          <Col className="mt-4 d-flex justify-content-center">
            <Form.Label className="text-white pt-2"><b>Category: </b></Form.Label>
            <Form.Control
              ref={categoryQuery}
              style={{ width: "10vw", marginLeft: "10px"}}
              type="search"
              className="me-2"
              aria-label="Search"
              defaultValue=""
              onChange={handleCategoryFilter}
            />
          </Col>
          <Col className="mt-4 d-flex justify-content-center">
            <Form.Label className="text-white pt-2"><b>Date Published: </b></Form.Label>
            <Form.Control
              ref={datePublishedQuery}
              style={{ width: "10vw", marginLeft: "10px"}}
              type="search"
              className="me-2"
              aria-label="Search"
              defaultValue={""}
              placeholder="YYYY-MM-DD"
              onChange={handlePublishedFilter}
            />
          </Col>
        </Row>
        <Row className="mx-auto text-center mt-4 mb-4">
          <Col>
            <Button
              variant="outline-light"
              onClick={() => setLoaded(false)}
            >
              Submit
            </Button>
          </Col>
        </Row>

      </Form>
      <Pagination className="justify-content-center mb-4">
        {activePage > 3 && (
          <Pagination.Item
            first
            key={1}
            onClick={() => handleClick(1)}
            active={1 === activePage}
          >
            1
          </Pagination.Item>
        )}
        {activePage > 4 && <Pagination.Ellipsis />}
        {items}
        {activePage < numPages - 3 && <Pagination.Ellipsis />}
        {activePage < numPages - 2 && (
          <Pagination.Item
            last
            key={numPages}
            onClick={() => handleClick(numPages)}
            active={numPages === activePage}
          >
            {numPages}
          </Pagination.Item>
        )}
      </Pagination>
      <div className="items-center text-center text-white">
        <p><b><i>Showing results {20 * (activePage - 1) + 1} - {20 * activePage} of {news && news.data && news.data.length}</i></b></p>
      </div>
      <Row
        xl={4}
        lg={3}
        md={1}
        sm={1}
        xs={1}
        className="d-flex g-4 p-4 justify-content-center"
      >
        { loaded && news.data ? (news.data.filter((_, i) => i >= (20 * (activePage - 1)) && i < (20 * activePage)).map((article) => (
          <Col key={article.id} className="d-flex align-self-stretch">
            <Card className="rounded">
              <Card.Body style={{ textAlign: "left" }}>
                <Card.Link href={"news/" + article.id}>
                  <Card.Img src={article.img_url !== "None" ? article.img_url : "https://designshack.net/wp-content/uploads/placeholder-image.png"}/>
                <p className='text-center'><a href={article.url}>View Official Article</a></p>
                </Card.Link>
                      <ul style={{ listStyle: "none", paddingLeft: "1rem"}}>
                      <li><b>Title</b>: {highlightText(article.title)}</li>
                      <li><b>Description</b>: {highlightText(article.description)}</li>
                      <li><b>Published</b>: {new Date(article.published).toDateString()}</li>
                      <li><b>Author</b>: <i>{highlightText(article.author)}</i></li>
                    </ul>
              </Card.Body>
            </Card>
          </Col>))) : <Spinner animation="border"/>}
      </Row>
      <Pagination className="justify-content-center mb-5">
        {activePage > 3 && (
          <Pagination.Item
            key={1}
            onClick={() => handleClick(1)}
            active={1 === activePage}
            first={true}
          >
            1
          </Pagination.Item>
        )}
        {activePage > 4 && <Pagination.Ellipsis />}
        {items}
        {activePage < numPages - 3 && <Pagination.Ellipsis />}
        {activePage < numPages - 2 && (
          <Pagination.Item
            key={numPages}
            onClick={() => handleClick(numPages)}
            active={numPages === activePage}
            last={true}
          >
            {numPages}
          </Pagination.Item>
        )}
      </Pagination>
    </div>
  )
}

export default News;
