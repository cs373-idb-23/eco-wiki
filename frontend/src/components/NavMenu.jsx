import React from "react";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Logo from "../assets/planet-earth.png";

function BoldText({ children }) {
  return (
    <span
      style={{
        fontSize: "20px",
        color: "#f4f1de",
        font: "Courier-Oblique",
        fontWeight: 400,
      }}
    >
      {children}
    </span>
  );
}

const NavMenu = () => {
  return (
    <Navbar
      variant="dark"
      expand="lg"
      style={{
        backgroundColor: "#185E3F",
      }}
    >
      <Container fluid className="mx-5">
        <Navbar.Brand href="/" className="navbar-brand">
          <div
            style={{
              display: "flex",
              justifyContent: "space-evenly",
              alignItems: "center",
            }}
          >
            <img src={Logo} className="img-fluid" alt="Logo"></img>
            <h2>EcoWiki</h2>
          </div>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse>
          <Nav>
            <Nav.Link href="/">
              <BoldText>Home</BoldText>
            </Nav.Link>
            <Nav.Link href="/about">
              <BoldText>About</BoldText>
            </Nav.Link>
            <Nav.Link href="/news">
              <BoldText>News</BoldText>
            </Nav.Link>
            <Nav.Link href="/policies">
              <BoldText>Policies</BoldText>
            </Nav.Link>
            <Nav.Link href="/community">
              <BoldText>Community</BoldText>
            </Nav.Link>
            <Nav.Link href="/visualizations">
              <BoldText>Visualizations</BoldText>
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default NavMenu;
