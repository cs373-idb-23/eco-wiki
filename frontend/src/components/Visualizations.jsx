import React from 'react'
import Container from "react-bootstrap/Container";
import Typography from "@mui/material/Typography";
import PoliciesPerState from './policy/PoliciesPerState';
import LikesPerPost from './community/LikesPerPost';
import ArticleCategoriesCount from './newsInstances/ArticleCategoriesCount';
import DrugsPerRoute from './care_connect/DrugsPerRoute';
import IllnessesPerRegion from './care_connect/IllnessesPerRegion';
import HealthProvidersPerType from './care_connect/HealthProvidersPerType';

function Visualizations() {
    return (
        <div style={{ backgroundColor: '#abd1c6', paddingTop:"15px"}}>
            <Container>
                <Typography
                variant="h3"
                sx={{ textAlign: "center" }}
                style={{
                    padding: "15px"
                }}>
                    Visualizations
                </Typography>
                <PoliciesPerState />
                <LikesPerPost />
                <ArticleCategoriesCount />
                <Typography
                variant="h3"
                sx={{ textAlign: "center" }}
                style={{
                    padding: "15px"
                }}>
                    Provider Visualizations
                </Typography>
                <DrugsPerRoute />
                <IllnessesPerRegion />
                <HealthProvidersPerType/>
            </Container>
        </div>
    )
}

export default Visualizations