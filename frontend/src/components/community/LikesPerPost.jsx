import React from "react";
import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const data = [
    {
        name: "> 1000",
        count: 15,
    },
    {
        name: "500 - 1000",
        count: 26,
    },
    {
        name: "250 - 499",
        count: 35,
    },
    {
        name: "100 - 249",
        count: 37,
    },
    {
        name: "50 - 99",
        count: 38,
    },
    {
        name: "10 - 49",
        count: 85,
    },
    {
        name: "0 - 10",
        count: 321,
    }
];

const LikesPerPost = () => {
    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center" style={{ color:"#185E3F" }}>Engagement Count in Posts</h3>
                <Col>
                <ResponsiveContainer width="100%" height="100%">
                    <LineChart
                        width={500}
                        height={300}
                        data={data}
                        margin={{
                            top: 5,
                            right: 30,
                            left: 20,
                            bottom: 5,
                        }}
                        >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Line type="monotone" dataKey="count" stroke="#185E3F" activeDot={{ r: 8 }} />
                    </LineChart>
                </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );
}

export default LikesPerPost;
