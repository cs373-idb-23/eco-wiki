import React, { useState, useEffect } from "react";
import {Link} from "react-router-dom";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Spinner } from "react-bootstrap";
import { Card, Button } from "react-bootstrap";

const client = axios.create({
  baseURL: "https://api.ecowiki.me/",
});

const Community = () => {

  const { id } = useParams();
  const [community, setCommunity] = useState();
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const fetchCommunity = async () => {
      if (community === undefined) {
        await client
          .get(`posts/id=${id}`)
          .then((response) => {
            setCommunity(response.data);
          })
          .catch((err) => console.log(err));
      }
      setLoaded(true);
    };
    fetchCommunity();
  }, [community]);

  return (
    <div>
      { loaded ? (<div className="container-fluid">
        <div className="row justify-content-center mt-5">
          <div className="col-4">
            <img
            src = {community.post.image_url ? community.post.image_url : "https://upload.wikimedia.org/wikipedia/commons/9/95/Instagram_logo_2022.svg"}
            alt=""/>
          </div>
          <div className="col-6">
            <div className="card">
                <div className="card-body">
                  <h4 className="card-title">Post Info <span><a target="_blank" href={community.post.url}>(Post link)</a></span></h4>
                  <ul style={{ listStyle: "none"}}>
                    <li><b>Caption</b>: {community.post.caption}</li>
                    <li><b>Date Posted</b>: {new Date(community.post.date).toDateString()}</li>
                    <li><b>Likes</b>: {community.post.likes}</li>
                    <li><b>Comments</b>: {community.post.comments}</li>
                    <li><b>Hashtags</b>: {community.post.hashtags}</li>
                    <li><b>Mentions</b>: {community.post.mentions ? community.post.mentions : "None"}</li>
                  </ul>
              </div>
          </div>
        </div>
      </div>
      <div className="row justify-content-center mt-5">
        { community.articles.length != 0 ? (
          <div className="justify-content-center text-center d-flex flex-column"> 
          <h5 className="bg-white p-2 mx-5 rounded"> Related Article(s)</h5>
          <div className="d-flex flex-wrap px-5 justify-content-center">
          { community.articles.map((article) => (
                <Card className="col-3 rounded p-3 m-3">
                <Card.Title>{article.title ? article.title : "None"} (<a href={"/news/" + article.id}>View Article</a>)</Card.Title>
                <Card.Body style={{ textAlign: "left" }}>
                    <Card.Link href={"news/" + article.id}>
                      <Card.Img src={article.img_url != "None" ? article.img_url : "https://designshack.net/wp-content/uploads/placeholder-image.png"}/>
                      <p className='text-center'><a href={article.url}>View Official Article</a></p>
                    </Card.Link>
                      <ul style={{ listStyle: "none", paddingLeft: "1rem"}}>
                        <li><b>Description</b>: {article.description}</li>
                        <li><b>Published</b>: {new Date(article.published).toDateString()}</li>
                        <li><b>Author</b>: <i>{article.author}</i></li>
                      </ul>
                </Card.Body>
              </Card>))}
          </div>
        </div>) : (<div></div>)}
      </div>
      <div className="row justify-content-center mt-5">
        { community.bills.length != 0 ? (
          <div className="justify-content-center text-center d-flex flex-column">
            <h5 className="bg-white p-2 mx-5 rounded"> Related Bill(s) </h5>
            <div className="d-flex flex-wrap px-5 justify-content-center">
            { community.bills.map((bill) => (
              <Card className="col-3 rounded p-3 m-3">
                <Card.Title>{bill.title ? bill.title : "None"} (<a href={"/policies/" + bill.id}>View Bill</a>)</Card.Title>
                <Card.Body style={{ textAlign: "left" }}>
                  <Card.Link href={"policies/" + bill.id}>
                    <Card.Img src={bill.image_url ? bill.image_url : "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Seal_of_the_United_States_House_of_Representatives.svg/600px-Seal_of_the_United_States_House_of_Representatives.svg.png?20110930013415"}/>
                  <p className='text-center'><a href={bill.url}>View Official Bill</a></p>
                  </Card.Link>
                        <ul style={{ listStyle: "none", paddingLeft: "1rem"}}>
                        <li><b>Bill Name</b>: {bill.title}</li>
                        <li><b>Description</b>: {bill.description}</li>
                        <li><b>Date</b>: {new Date(bill.date).toDateString()}</li>
                        <li><b>Sponsor(s)</b>: <i>{bill.sponsor}</i></li>
                      </ul>
                </Card.Body>
              </Card>))}
            </div>
          </div>) : (<div></div>)}         
        </div>
    </div>) : (<Spinner animation="border" />)}
    </div>
  );
}

export default Community;
