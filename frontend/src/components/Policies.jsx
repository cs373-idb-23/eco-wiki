import React, { useEffect, useState, useRef } from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import FilterDropdown from "../components/FilterDropdown";
import Pagination from "react-bootstrap/Pagination";
import Spinner from "react-bootstrap/Spinner";
import axios from "axios";
import { Highlight } from "react-highlight-regex";


const client = axios.create({
  baseURL: "https://api.ecowiki.me/",
});

var queryRE = null;

function Policies() {
  const searchQuery = useRef("");
  const numCoSponsorsQuery = useRef("#")
  const dateIntroducedQuery = useRef("YYYY-MM-DD")

  const [policies, setPolicies] = useState([]);
  const [activePage, setActivePage] = useState(1);
  const [loaded, setLoaded] = useState(false);

  const [sort, setSort] = useState("sort");
  const [ascending, setAscending] = useState(false);
  const [sponsorState, setSponsorState] = useState("Sponsor State");
  const [sponsorParty, setSponsorParty] = useState("Sponsor Party");
  const [status, setStatus] = useState("Status");
  const [introduced, setIntroduced] = useState("Introduced");
  const [numCoSponsors, setNumCoSponsors] = useState(0);
  
  const handleSortFilter = (value) => {
    setSort(value.toLowerCase().replace(" ", "_"));
  };
  const handleOrderFilter = (value) => {
    setAscending(value === "Ascending");
  };
  const handleSponsorStateFilter = (value) => {
    setSponsorState(value);
  };
  const handleSponsorPartyFilter = (value) => {
    setSponsorParty(value);
  };
  const handleIntroducedFilter = (value) => {
    setIntroduced(value);
  };
  const handleActiveFilter = (value) => {
    setStatus(value);
  };
  const handleNumCoSponsorsFilter = (value) => {
    setNumCoSponsors(value);
  };
  function handleClick(number) {
    setActivePage(number);
    setLoaded(false);
  }

  function highlightText(input) {
    if (queryRE != null) {
      return <Highlight match={queryRE} text={input} />;
    }
    return input;
  }


  useEffect(() => {
    const fetchBills = async () => {
      if (!loaded) {
        var query = `bills`;
        if (searchQuery.current.value !== "") {
          query = `search/bills/${searchQuery.current.value}`;
          queryRE = new RegExp(
            `(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
            "i"
          );
        } else {
          queryRE = null;
          query += '?'
          if (sort !== "sort") {
            query += `&sort=${sort}`;
          }
          if (ascending && sort !== "sort") {
            query += "&asc";
          }
          if (sponsorParty !== "Sponsor Party") {
            query += `&sponsorParty=${sponsorParty}`;
          }
          if (sponsorState !== "Sponsor State") {
            query += `&sponsorState=${sponsorState}`;
          }
          if (numCoSponsorsQuery.current.value !== "") {
            query += `&numCosponsors=${numCoSponsorsQuery.current.value}`;
          } 
          if (dateIntroducedQuery.current.value !== "") {
            query += `&introduced=${dateIntroducedQuery.current.value}`;
          }
          if (status !== "Status") {
            query += `&status=${status}`;
          }
          if (introduced !== "Introduced") {
            query += `&introduced=${introduced}`;
          }
        }

        console.log(query);
        await client
          .get(query)
          .then((response) => {
            setPolicies(response.data)
            // setNumPages(Math.Ceil(policies.count / 20))
            console.log(policies.data)
          })
          .catch((err) => console.log(err));
        setLoaded(true);
      }
    };
    fetchBills();
  }, [policies, ascending, introduced, numCoSponsors, sort, sponsorParty, sponsorState, status, loaded]);

  let numPages = Math.ceil(865 / 20)
  let items = [];

  for (let number = activePage - 2; number <= activePage + 2; number++) {
    if (number > 0 && number <= numPages) {
      items.push(
        <Pagination.Item
          key={number}
          onClick={() => handleClick(number)}
          active={number === activePage}
        >
          {number}
        </Pagination.Item>
      );
    }
  }

  return (
    <div className="container">
      <div className="text-center my-4">
        <h1 className= "title">Policies</h1>
        <p>Discover the government policies shaping our planet's future.</p>
      </div>
      <Form
        onSubmit={(event) => {
          event.preventDefault();
          setLoaded(false);
        }}
        className="d-flex pb-2 justify-content-center"
      >
        <Form.Control
          ref={searchQuery}
          style={{ width: "20vw" }}
          type="search"
          placeholder="Search Bills"
          className="me-2"
          aria-label="Search"
        />
        <Button variant="outline-light" onClick={() => setLoaded(false)}>
          Search
        </Button>
      </Form>
      <Form className="filter-form">
        <Row className="mx-auto text-center w-50 my-4">
          <Col>
            <FilterDropdown
              title="Sort"
              items={["Sort", "Sponsor State", "Sponsor Party", "Introduced", "Active"]}
              onChange={handleSortFilter}
            />
          </Col>
          <Col>
            <FilterDropdown
              title="Order"
              items={["Ascending", "Descending"]}
              onChange={handleOrderFilter}
            />
          </Col>
          <Col>
            <FilterDropdown
              title="Sponsor Party"
              items={["Sponsor Party", "D", "R", "I"]}
              onChange={handleSponsorPartyFilter}
            />
          </Col>
          <Col>
            <FilterDropdown
              title="Status"
              items={["Status", "active", "inactive", "pending"]}
              onChange={handleActiveFilter}
            />
          </Col>
          <Col>
            <FilterDropdown
              title="State"
              items={[
                "AK", "AL", "AR", "AS", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MI", "MN", "MO", "MP", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UM", "UT", "VA", "VI", "VT", "WA", "WI", "WV", "WY"
              ]}
              onChange={handleSponsorStateFilter}
            />
          </Col>
        </Row>
        <Row>
          <Col className="mt-4 d-flex justify-content-center">
            <Form.Label className="text-white pt-2"><b>Number of CoSponsors: </b></Form.Label>
            <Form.Control
              ref={numCoSponsorsQuery}
              style={{ width: "10vw", marginLeft: "10px"}}
              type="search"
              className="me-2"
              aria-label="Search"
              defaultValue=""
              placeholder="#"
              onChange={handleNumCoSponsorsFilter}
            />
          </Col>
          <Col className="mt-4 d-flex justify-content-center">
            <Form.Label className="text-white pt-2"><b>Date Introduced: </b></Form.Label>
            <Form.Control
              ref={dateIntroducedQuery}
              style={{ width: "10vw", marginLeft: "10px"}}
              type="search"
              className="me-2"
              aria-label="Search"
              defaultValue={""}
              placeholder="YYYY-MM-DD"
              onChange={handleIntroducedFilter}
            />
          </Col>
        </Row>
        <Row className="mx-auto text-center mt-4 mb-4">
          <Col>
            <Button
              variant="outline-light"
              onClick={() => setLoaded(false)}
            >
              Submit
            </Button>
          </Col>
        </Row>

      </Form>
      <div className='cards-container'>
      <Pagination className="justify-content-center mb-4">
        {activePage > 3 && (
          <Pagination.Item
            first
            key={1}
            onClick={() => handleClick(1)}
            active={1 === activePage}
          >
            1
          </Pagination.Item>
        )}
        {activePage > 4 && <Pagination.Ellipsis />}
        {items}
        {activePage < numPages - 3 && <Pagination.Ellipsis />}
        {activePage < numPages - 2 && (
          <Pagination.Item
            last
            key={numPages}
            onClick={() => handleClick(numPages)}
            active={numPages === activePage}
          >
            {numPages}
          </Pagination.Item>
        )}
      </Pagination>
      <div className="items-center text-center text-white">
        <p><b><i>Showing results {20 * (activePage - 1) + 1} - {20 * activePage} of {policies && policies.data && policies.data.length}</i></b></p>
      </div>
      <Row
        xl={4}
        lg={3}
        md={1}
        sm={1}
        xs={1}
        className="d-flex g-4 p-4 justify-content-center"
      >
        { loaded && policies.data ? (policies.data.filter((_, i) => i >= (20 * (activePage - 1)) && i < (20 * activePage)).map((policy) => (
          <Col key={policy.id} className="d-flex align-self-stretch">
            <Card className="rounded">
              <Card.Body style={{ textAlign: "left" }}>
                <Card.Link href={"policies/" + policy.id}>
                  <Card.Img src={policy.image_url ? policy.image_url : "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Seal_of_the_United_States_House_of_Representatives.svg/600px-Seal_of_the_United_States_House_of_Representatives.svg.png?20110930013415"}/>
                <p className='text-center'><a href={policy.url}>View Official Bill</a></p>
                </Card.Link>
                    <p className='text-center'><i>{policy.status}</i></p>
                    <ul style={{ listStyle: "none", paddingLeft: "1rem"}}>
                      <li><b>Bill Name</b>: {highlightText(policy.title)}</li>
                      <li><b>Sponsor Party</b>: {highlightText(policy.sponsor_party)}</li>
                      <li><b>Sponsor State</b>: {policy.sponsor_state}</li>
                      <li><b>Date</b>: {highlightText(policy.introduced)}</li>
                      <li><b>Sponsor(s)</b>: <i>{policy.sponsor}</i></li>
                      <li><b>Number of CoSponsor(s)</b>: {policy.num_cosponsors}</li>
                    </ul>
              </Card.Body>
            </Card>
          </Col>))) : (
            <Spinner animation="grow" />
          )
        }
        </Row>
        <Pagination className="justify-content-center mb-5">
        {activePage > 3 && (
          <Pagination.Item
            key={1}
            onClick={() => handleClick(1)}
            active={1 === activePage}
            first={true}
          >
            1
          </Pagination.Item>
        )}
        {activePage > 4 && <Pagination.Ellipsis />}
        {items}
        {activePage < numPages - 3 && <Pagination.Ellipsis />}
        {activePage < numPages - 2 && (
          <Pagination.Item
            key={numPages}
            onClick={() => handleClick(numPages)}
            active={numPages === activePage}
            last={true}
          >
            {numPages}
          </Pagination.Item>
        )}
      </Pagination>
      </div>
    </div>
  );
}

export default Policies;
