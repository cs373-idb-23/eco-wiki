import React from 'react'

function Card({title, author, date, description, image, link}) {
    
    return (
        <div className='card-container' onClick={() => window.location.pathname = link}>
            <div className='whole-card'>
                <div className='top-card'>
                    <div className='image-container'>
                        <img className='logo'src={image} alt=''/>
                    </div>
                </div>
                <div className='bottom-card'>
                    <div className='card-content'>
                        <div className='card-title'>
                            <h3>{title}</h3>
                        </div>
                        <div className='card-description'>
                            <p>{description}</p>
                        </div>
                        <div className='card-author'>
                            <p>{author}</p>
                            <p>{date}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Card