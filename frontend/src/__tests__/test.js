import Card from "../components/Card";
import App from "../App";
import NavMenu from "../components/NavMenu";
import Home from "../components/Home";
import About from "../components/About";
import News from "../components/News";
import Policies from "../components/Policies";
import Community from "../components/Communities";
import InstanceCard from "../components/newsInstances/InstanceCard";
import NewsInstance from "../components/newsInstances/NewsInstance";
import FilterDropdown from "../components/FilterDropdown";
import { BrowserRouter } from "react-router-dom";

describe('Main pages', () => {
  test('Home', () => {
      const tree = <BrowserRouter>renderer.create(<Home />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
  })

  test('News', () => {
      const tree = <BrowserRouter>renderer.create(<News />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
  })

  test('Policies', () => {
      const tree = <BrowserRouter>renderer.create(<Policies />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
  })

  test('Community', () => {
      const tree = <BrowserRouter>renderer.create(<Community />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
  })

  test('About', () => {
      const tree = <BrowserRouter>renderer.create(<About />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
  })

  test('Home', () => {
    const tree = <BrowserRouter>renderer.create(<Home />).toJSON()</BrowserRouter>
    expect(tree).toMatchSnapshot()
})

})

describe('Page structures', () => {

  test('Card', () => {
      const tree = <BrowserRouter>renderer.create(<Card />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
  });

  test('Navigation Bar', () => {
      const tree = <BrowserRouter>renderer.create(<NavMenu />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
  });

  test('News Instance Card', () => {
      const tree = <BrowserRouter>renderer.create(<InstanceCard />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
  });

  test('News Instance', () => {
      const tree = <BrowserRouter>renderer.create(<NewsInstance />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
  });

  test('Filter Dropdown', () => {
    const tree = <BrowserRouter>renderer.create(<FilterDropdown />).toJSON()</BrowserRouter>
    expect(tree).toMatchSnapshot()
})

  test('Main App', () => {
      const tree = <BrowserRouter>renderer.create(<App />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
  });
})



