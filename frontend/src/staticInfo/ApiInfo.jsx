import newsdataLogo from '../images/apisIcon/newsdataLogo.png'
import igLogo from '../images/apisIcon/igLogo.png'
import gitlabLogo from '../images/apisIcon/gitlabLogo.png'
import propublica from '../images/apisIcon/propublica.png'
import google from '../images/apisIcon/google.png'

// inspired by GeoJobs project (https://www.geojobs.me/about)

const ApiInfo = [
    {
        name: 'Newsdata API',
        description: 'NewsData is a news API that provides access to the latest news from thousands of sources with various filters and categories to choose from.',
        url: 'https://newsdata.io',
        img: newsdataLogo,
    },
    {
        name: 'Instagram API',
        description: 'The Instagram API is a RESTful API that allows you to access Instagram data like posts and comments by users and hashtags relating to environmental science.',
        url: 'https://www.instagram.com/developer/',
        img: igLogo,
    },
    {
        name: 'ProPublica API',
        description: 'ProPublica is a RESTful API that provides near real-time access to legislative data from the House of Representatives, the Senate and the Library of Congress.',
        url: 'https://projects.propublica.org/api-docs/congress-api/',
        img: propublica
    },
    {
        name: 'Gitlab API',
        description: 'Gitlab is a RESTful API that allows you to access information about commits, issues, and merge requests in the Gitlab repository.',
        url: 'https://docs.gitlab.com/ee/api/',
        img: gitlabLogo
    },
    {
        name: 'Google Custom Search API',
        description: 'Google Custom Search is a RESTful API that allows you to request either web search or image search results in JSON format.',
        url: 'https://developers.google.com/custom-search/v1/overview',
        img: google
    },
]

export { ApiInfo }