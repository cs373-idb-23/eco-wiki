import gitlabLogo from '../images/apisIcon/gitlabLogo.png'

const ToolsInfo = [
    {
        name: 'React',
        description: 'React is a JavaScript library for building user interfaces.',
        url: 'https://reactjs.org/',
        img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1200px-React-icon.svg.png',
    },
    {
        name: 'React Bootstrap',
        description: 'Integrates the bootstrap CSS library for React components.',
        url: 'https://react-bootstrap.github.io/',
        img: 'https://react-bootstrap.github.io/logo.svg',
    },
    {
        name: 'Postman API',
        description: 'Tool for API development, testing, and documentation.',
        url: 'https://www.postman.com/',
        img: 'https://user-images.githubusercontent.com/7853266/44114706-9c72dd08-9fd1-11e8-8d9d-6d9d651c75ad.png',
    },
    {
        name: 'VS Code',
        description: 'Text Editor & an IDE for developing software.',
        url: 'https://code.visualstudio.com/',
        img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/1200px-Visual_Studio_Code_1.35_icon.svg.png',
    },
    {
        name: 'Namecheap',
        description: 'Domain name registrar and web hosting company.',
        url: 'https://www.namecheap.com/',
        img: 'https://cdn.cdnlogo.com/logos/n/24/namecheap.svg',
    },
    {
        name: 'Microsoft Teams',
        description: 'Collaboration platform for chat, meetings, calling, and more.',
        url: 'https://www.microsoft.com/en-us/microsoft-365/microsoft-teams/group-chat-software',
        img: 'https://download.logo.wine/logo/Microsoft_Teams/Microsoft_Teams-Logo.wine.png',
    },
    {
        name: 'Gitlab',
        description: 'Git repository hosting service for version control and collaboration.',
        url: 'https://gitlab.com/',
        img: gitlabLogo,
    },
    {
        name: 'Discord',
        description: 'Voice, video, and text communication service.',
        url: 'https://discord.com/',
        img: 'https://logos-world.net/wp-content/uploads/2020/12/Discord-Emblem.png',
    },
    {
        name: 'AWS Amplify',
        description: 'Hosting platform provided by Amazon Web Services.',
        url: 'https://aws.amazon.com/amplify/',
        img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Amazon_Web_Services_Logo.svg/1200px-Amazon_Web_Services_Logo.svg.png',
    }
]

export { ToolsInfo }