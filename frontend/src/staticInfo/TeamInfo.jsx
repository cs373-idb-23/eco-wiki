// Import headshots below
import AayushImg from '../images/teamHeadshots/aayush.jpg'
import EduardoImg from '../images/teamHeadshots/eduardo.jpg'
import ImranImg from '../images/teamHeadshots/imran.png'
import ShazImg from '../images/teamHeadshots/shaz.png'
import YundiImg from '../images/teamHeadshots/yundi.png'

// inspired by GeoJobs project (https://www.geojobs.me/about)

const teamInfo = [
    {
        name: 'Aayush Gupta',
        bio: 'I am a junior at the University of Texas at Austin majoring in Computer Science. For this project, I am working on backend.',
        img: AayushImg,
        id: 'mugloos',
        commits: 0,
        issues: 0,
        unit_tests: 19
    }, 
    {
        name: 'Eduardo Cazares',
        bio: 'I am a junior at the University of Texas at Austin majoring in Computer Science. For this project, I am working on backend and AWS.',
        img: EduardoImg,
        id: 'Ecazares31',
        commits: 0,
        issues: 0,
        unit_tests: 0
    },
    {
        name: 'Imran Hussein',
        bio: 'I am a junior at the University of Texas at Austin majoring in Computer Science. For this project, I am working on frontend.',
        img: ImranImg,
        id: 'imranh02',
        commits: 0,
        issues: 0,
        unit_tests: 10
    },
    {
        name: 'Shaz Momin',
        bio: 'I am a junior at the University of Texas at Austin majoring in Computer Science. For this project, I am working on frontend.',
        img: ShazImg,
        id: 'shaz.momin',
        commits: 0,
        issues: 0,
        unit_tests: 10
    },
    {
        name: 'Yundi Li',
        bio: 'I am a junior at the University of Texas at Austin majoring in Computer Science. For this project, I am working on backend.',
        img: YundiImg,
        id: 'rollingthundar',
        commits: 0,
        issues: 0,
        unit_tests: 10
    }
]

export { teamInfo }