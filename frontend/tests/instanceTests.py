import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

URL = "https://www.ecowiki.me/"

class Test(unittest.TestCase):

  @classmethod
  def setUpClass(self) -> None:
    options = webdriver.ChromeOptions()
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    options.add_argument("--headless")
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-dev-shm-usage")
    chrome_prefs = {}
    options.experimental_options["prefs"] = chrome_prefs
    # Disable images
    chrome_prefs["profile.default_content_settings"] = {"images": 2}
    self.driver = webdriver.Chrome(options=options, service=Service(ChromeDriverManager().install()))
    self.driver.get(URL)

  @classmethod
  def tearDownClass(self):
    self.driver.quit()

  def test_job(self):
    self.driver.get(URL + "news")
    self.assertEqual(self.driver.current_url, URL + "news")
  
  def test_city(self):
    self.driver.get(URL + "policies")
    self.assertEqual(self.driver.current_url, URL + "policies")

  def test_community(self):
    self.driver.get(URL + "community")
    self.assertEqual(self.driver.current_url, URL + "community")

if __name__ == '__main__':
    unittest.main()