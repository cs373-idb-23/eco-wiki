from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

# problica
# instagram
# newscenter

app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://admin:rI5iEbWjvR1lX93CKkor@ecowiki-database.ctbrniljsq6h.us-east-2.rds.amazonaws.com/ecowiki'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    caption = db.Column(db.Text)
    likes = db.Column(db.BigInteger)
    comments = db.Column(db.BigInteger)
    url = db.Column(db.Text)
    date = db.Column(db.Date)
    hashtags = db.Column(db.Text)
    mentions = db.Column(db.Text)
    tag = db.Column(db.String(50))
    image_url = db.Column(db.Text)
    bills = db.relationship('Bill', backref='post')
    articles = db.relationship('Article', backref='post')

class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    post_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    description = db.Column(db.Text)
    url = db.Column(db.Text)
    author = db.Column(db.String(200))
    img_url = db.Column(db.String(500)) 
    language = db.Column(db.String(20))
    category = db.Column(db.String(200))
    published = db.Column(db.DateTime)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))

class Bill(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    post_id = db.Column(db.Integer, primary_key=True)
    bill_id = db.Column(db.String(20))
    title = db.Column(db.Text)
    description = db.Column(db.Text)
    url = db.Column(db.Text)
    introduced = db.Column(db.Date)
    sponsor = db.Column(db.Text)
    sponsor_state = db.Column(db.String(10))
    sponsor_party = db.Column(db.String(10))
    status = db.Column(db.String(10))
    num_cosponsors = db.Column(db.Integer)
    dem_cosponsors = db.Column(db.Integer)
    rep_cosponsors = db.Column(db.Integer)
    ind_cosponsors = db.Column(db.Integer)
    summary = db.Column(db.Text)
    subjects = db.Column(db.Text)
    committee = db.Column(db.Text)
    image_url = db.Column(db.Text)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))