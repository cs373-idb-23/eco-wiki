import json
from models import app, db, Post, Article, Bill
import random
import string

ignored_words = ["and", "the", "for", "into", "all", "its", "how", "are", "from"]

def populate_db():
    populate_posts()
    populate_articles()
    populate_bills()

def populate_posts():
    with open("data/instagram_scrape.json") as jsn:
        posts = json.load(jsn)
        for i in range(7) :
            tag = posts[i]['name']
            for post in posts[i]['topPosts']:   
                db_row = {
                    'caption':post['caption'],
                    'likes':post['likesCount'],
                    'comments':post['commentsCount'],
                    'url':post['url'],
                    'date':post['timestamp'][0:10],
                    'hashtags':', '.join(post['hashtags']),
                    'mentions':', '.join(post['mentions']),
                    'tag':tag,
                    'image_url':post['image_url']
                }
                db.session.add(Post(**db_row))
            for post in posts[i]['latestPosts']:   
                db_row = {
                    'caption':post['caption'],
                    'likes':post['likesCount'],
                    'comments':post['commentsCount'],
                    'url':post['url'],
                    'date':post['timestamp'][0:10],
                    'hashtags':', '.join(post['hashtags']),
                    'mentions':', '.join(post['mentions']),
                    'tag':tag,
                    'image_url':post['image_url']
                }
                db.session.add(Post(**db_row))
            
        db.session.commit()    

def populate_articles():
    files = ["data/article_scrape1.json", "data/article_scrape2.json", "data/article_scrape3.json"]
    for data in files:
        with open(data) as jsn:
            articles = json.load(jsn)
            for article in articles['news']:
                date = article['published'].split(' +')
                db_row = {
                    'title':article['title'],
                    'description':article['description'],
                    'url':article['url'],
                    'author':article['author'],
                    'img_url':article['image'],
                    'language':article['language'],
                    'category':', '.join(article['category']),
                    'published':date[0],
                    # 'post_id':random.randint(1,500)
                }
                db.session.add(Article(**db_row))
            db.session.commit()

def populate_bills():
    statusVals = ["active", "inactive", "pending"]
    with open("data/propublica_scrape.json") as jsn:
        results = json.load(jsn)
        # for i in range(10):
        for result in results:
            # result = results[i]
            bills = result['results']
            for bill in bills:
                subject = result['subject']
                isSame = False
                if bill['active'] == None:
                    status = "pending"
                elif bill['active'] == True:
                    status = "active"
                else:
                    status = "inactive"
                if bill['primary_subject'] != None:
                    subject += ", " + bill['primary_subject']
                dems, reps, inds = assign_cosponser_party(bill['cosponsors_by_party'])
                if bill['short_title'] == bill['title']:
                    isSame = True
                if not isSame and bill['short_title'] != None and bill['summary_short'] != None and bill['committees'] != None:
                    db_row = {
                        'bill_id':bill['bill_id'],
                        'title':bill['short_title'],
                        'description':bill['title'],
                        'url':bill['congressdotgov_url'],
                        'introduced':bill['introduced_date'],
                        'sponsor':bill['sponsor_name'],
                        'sponsor_state':bill['sponsor_state'],
                        'sponsor_party':bill['sponsor_party'],
                        'status':status,
                        'num_cosponsors':bill['cosponsors'],
                        'dem_cosponsors':dems,
                        'rep_cosponsors':reps,
                        'ind_cosponsors':inds,
                        'summary':bill['summary_short'],
                        'subjects':subject,
                        'committee':bill['committees'],
                        'image_url':bill['image_url'],
                        # 'post_id':random.randint(1,500)
                    }
                    db.session.add(Bill(**db_row))
        db.session.commit()

def assign_cosponser_party(parties):
    dems = 0
    reps = 0
    inds = 0
    if parties.get('D') != None:
        dems = parties['D']
    if parties.get('R') != None:
        reps = parties['R']
    if parties.get('ID') != None:
        inds = parties['ID']
    return dems, reps, inds

# Work in Progress
def relate_posts_bills():
    post_query = Post.query.filter(Post.hashtags != "")
    bill_query = Bill.query.filter(Bill.subjects != "")

    post_list = []
    bill_list = []

    for post in post_query:
        ht = post.hashtags
        cap = post.caption
        id = post.id
        for punctuation in string.punctuation:
            ht = ht.replace(punctuation, '').lower()
            cap = cap.replace(punctuation, '').lower()
        ht = ' '.join(ht.split())
        cap = ' '.join(cap.split())
        words = str(id) + ' ' + ht + ' ' + cap
        post_list.append(words.split())

    for bill in bill_query:
        tit = bill.title
        sub = bill.subjects
        for punctuation in string.punctuation:
            tit = tit.replace(punctuation, '').lower()
            sub = sub.replace(punctuation, '').lower()
        tit = ' '.join(tit.split())
        sub = ' '.join(sub.split())
        words = tit + ' ' + sub
        bill_list.append(words.split())

    id_list = []
    for bill_words in bill_list:
        bill_dict = {}
        max_occ = 0
        best_id = 0
        for i in range(len(post_list)):
            for word in bill_words:
                if len(word) > 2 and word not in ignored_words:
                    bill_dict[word] = 0

            temp_list = post_list[i]
            post_id = temp_list[0]
            for i in range(1, len(temp_list)):
                term = temp_list[i]
                for comp in bill_dict.keys():
                    if comp in term:
                        bill_dict[comp] += 1
            occ = 0
            lim = 0
            bill_dict = dict(sorted(bill_dict.items(), key=lambda x:x[1], reverse=True))
            for value in bill_dict.values():
                if lim <= 5:
                    occ += value
                else:
                    break
                lim += 1
            if occ > max_occ:
                max_occ = occ
                best_id = post_id

        id_list.append(best_id)

    bill_id = 1
    for id in id_list:
        bill = Bill.query.get(bill_id)
        if id != 0:
            bill.post_id = id
        else:
            bill.post_id = random.randint(1,100)
        bill_id += 1

    db.session.commit()    

def relate_posts_articles():
    post_query = Post.query.filter(Post.hashtags != "")
    article_query = Article.query.filter(Article.title != "")

    post_list = []
    article_list = []

    for post in post_query:
        ht = post.hashtags
        cap = post.caption
        id = post.id
        for punctuation in string.punctuation:
            ht = ht.replace(punctuation, '').lower()
            cap = cap.replace(punctuation, '').lower()
        ht = ' '.join(ht.split())
        cap = ' '.join(cap.split())
        words = str(id) + ' ' + ht + ' ' + cap
        post_list.append(words.split())

    for article in article_query:
        tit = article.title
        cat = article.category
        for punctuation in string.punctuation:
            tit = tit.replace(punctuation, '').lower()
            cat = cat.replace(punctuation, '').lower()
        tit = ' '.join(tit.split())
        cat = ' '.join(cat.split())
        words = tit + ' ' + cat
        article_list.append(words.split())

    id_list = []
    for article_words in article_list:
        article_dict = {}
        max_occ = 0
        best_id = 0
        for i in range(len(post_list)):
            for word in article_words:
                if len(word) > 2 and word not in ignored_words:
                    article_dict[word] = 0

            temp_list = post_list[i]
            post_id = temp_list[0]
            for i in range(1, len(temp_list)):
                term = temp_list[i]
                for comp in article_dict.keys():
                    if comp in term:
                        article_dict[comp] += 1
            occ = 0
            lim = 0
            article_dict = dict(sorted(article_dict.items(), key=lambda x:x[1], reverse=True))
            for value in article_dict.values():
                if lim <= 5:
                    occ += value
                else:
                    break
                lim += 1
            if occ > max_occ:
                max_occ = occ
                best_id = post_id

        id_list.append(best_id)

    article_id = 1
    for id in id_list:
        article = Article.query.get(article_id)
        if id != 0:
            article.post_id = id
        else:
            article.post_id = random.randint(1,100)
        article_id += 1

    db.session.commit() 

if __name__ == "__main__":
    with app.app_context():
        db.drop_all()
        db.create_all()
        populate_db()
        relate_posts_bills()
        relate_posts_articles()
