import app
from unittest import main, TestCase

class Tests(TestCase):
    def setUp(self):
        app.app.config["TESTING"] = True
        self.client = app.app.test_client()
    
    def test_get_posts(self):
        with self.client:
            response = self.client.get("/posts")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 557)

    def test_get_articles(self):
        with self.client:
            response = self.client.get("/articles")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 600)

    def test_get_bills(self):
        with self.client:
            response = self.client.get("/bills")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 865)

    def test_posts_pagination(self):
        with self.client:
            response = self.client.get("/posts?page=1&numResults=9")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 9)

    def test_articles_pagination(self):
        with self.client:
            response = self.client.get("/articles?page=1&numResults=18")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 18)

    def test_bills_pagination(self):
        with self.client:
            response = self.client.get("/bills?page=1&numResults=27")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 27)

    def test_post_instance_1(self):
        with self.client:
            response = self.client.get("/posts/id=1")
            self.assertEqual(response.status_code, 200)
            data = response.json["post"]
            self.assertEqual(data["likes"], 4544)
            self.assertEqual(data["date"], "2023-02-25")
            hashtags = data["hashtags"].split(",")
            self.assertEqual(len(hashtags), 1)
    
    def test_post_instance_2(self):
        with self.client:
            response = self.client.get("/posts/id=69")
            self.assertEqual(response.status_code, 200)
            data = response.json["post"]
            self.assertEqual(data["likes"], 1)
            self.assertEqual(data["date"], "2023-02-27")
            hashtags = data["hashtags"].split(",")
            self.assertEqual(len(hashtags), 16)

    def test_article_instance_1(self):
        with self.client:
            response = self.client.get("/articles/id=3")
            self.assertEqual(response.status_code, 200)
            data = response.json["article"]
            self.assertEqual(data["language"], "en")
            self.assertEqual(data["author"], "Dan Robinson")
            self.assertEqual(data["published"], "2023-02-28T20:01:14")

    def test_article_instance_2(self):
        with self.client:
            response = self.client.get("/articles/id=100")
            self.assertEqual(response.status_code, 200)
            data = response.json["article"]
            self.assertEqual(data["language"], "en")
            self.assertEqual(data["author"], "Paul Lee")
            self.assertEqual(data["published"], "2023-02-28T03:51:11")

    def test_bill_instance_1(self):
        with self.client:
            response = self.client.get("/bills/id=10")
            self.assertEqual(response.status_code, 200)
            data = response.json["bill"]
            self.assertEqual(data["status"], "pending")
            self.assertEqual(data["title"], "National Hurricane Research Initiative Act of 2007")
            self.assertEqual(data["introduced"], "2007-03-20")

    def test_bill_instance_2(self):
        with self.client:
            response = self.client.get("/bills/id=120")
            self.assertEqual(response.status_code, 200)
            data = response.json["bill"]
            self.assertEqual(data["status"], "pending")
            self.assertEqual(data["title"], "Arctic Marine Shipping Assessment Implementation Act of 2010")
            self.assertEqual(data["introduced"], "2010-07-19")
    
    def test_search_all(self):
        with self.client:
            response = self.client.get("/search/earth")
            self.assertEqual(response.status_code, 200)
            bills = response.json["bills"]
            articles = response.json["articles"]
            posts = response.json["posts"]
            self.assertEqual(len(bills), 12)
            self.assertEqual(len(articles), 6)
            self.assertEqual(len(posts), 340)

    def test_search_bill(self):
        with self.client:
            response = self.client.get("/search/bills/conservation")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 177)

    def test_search_post(self):
        with self.client:
            response = self.client.get("/search/posts/recycle")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 16)

    def test_search_article(self):
        with self.client:
            response = self.client.get("/search/articles/global%20warming")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 47)
    
    def test_sort_bill_1(self):
        with self.client:
            response = self.client.get("/bills?sort=introduced")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 865)
            self.assertEqual(data[0]["id"], 450)
            self.assertEqual(data[119]["id"], 579)
            self.assertEqual(data[0]["introduced"], "2020-05-27")
            self.assertEqual(data[119]["introduced"], "2019-05-14")

    def test_sort_post_1(self):
        with self.client:
            response = self.client.get("/posts?sort=likes")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 557)
            self.assertEqual(data[0]["id"], 8)
            self.assertEqual(data[211]["id"], 211)
            self.assertEqual(data[0]["likes"], 9970)
            self.assertEqual(data[211]["likes"], 17)

    def test_sort_article_1(self):
        with self.client:
            response = self.client.get("/articles?sort=published")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 600)
            self.assertEqual(data[0]["id"], 1)
            self.assertEqual(data[199]["id"], 200)
            self.assertEqual(data[0]["published"], "2023-02-28T20:22:00")
            self.assertEqual(data[199]["published"], "2023-02-27T16:32:00") 
                
if __name__ == '__main__':
    main()