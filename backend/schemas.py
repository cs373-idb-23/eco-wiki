from models import Article, Post, Bill
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema 

class ArticleSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Article

class PostSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Post

class BillSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Bill
        