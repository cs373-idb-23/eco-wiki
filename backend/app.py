from models import app, db, Article, Post, Bill
from schemas import ArticleSchema, PostSchema, BillSchema
from flask import request, jsonify, Response
from sqlalchemy import or_, desc
import json

DEFAULT_PAGE_SIZE = 9
article_schema = ArticleSchema()
post_schema = PostSchema()
bill_schema = BillSchema()

@app.route("/")
def index():
    """
    :return: html rendered in home page of api
    """
    try:
        return "<h1>Ecowiki API</h1>"
    except Exception as e:
        error_text = "<p>The error:<br>" + str(e) + "</p>"
        hed = "<h1>Something is broken.</h1>"
        return hed + error_text
    
@app.route("/search/<string:query>")
def search_all(query):
    """
    Searches all models for matching terms from given query.

    :query: the query containing terms to search for
    :return: the results matching the terms
    """
    terms = query.split()
    occurrences = {
        **search_articles(terms),
        **search_posts(terms),
        **search_bills(terms)
    }
    objs = sorted(occurrences.keys(), key=lambda x: occurrences[x], reverse=True)
    articles = [article for article in objs if type(article) == Article]
    posts = [post for post in objs if type(post) == Post]
    bills = [bill for bill in objs if type(bill) == Bill]
    article_results = article_schema.dump(articles, many=True)
    post_results = post_schema.dump(posts, many=True)
    bill_results = bill_schema.dump(bills, many=True)
    return jsonify(
        {"articles": article_results, "posts": post_results, "bills": bill_results}
    )


@app.route("/search/<string:model>/<string:query>")
def search_by_model(model, query):
    """
    Searches the given model using the given query

    :model: the given model to search from
    :query: the terms to search for
    :return: the results matching the terms in the model
    """
    model = model.strip().lower()
    terms = query.split()
    result = None
    if model == "articles":
        occurrences = search_articles(terms)
        articles = sorted(occurrences.keys(), key=lambda x: occurrences[x], reverse=True)
        result = article_schema.dump(articles, many=True)
    elif model == "posts":
        occurrences = search_posts(terms)
        posts = sorted(occurrences.keys(), key=lambda x: occurrences[x], reverse=True)
        result = post_schema.dump(posts, many=True)
    elif model == "bills":
        occurrences = search_bills(terms)
        bills = sorted(occurrences.keys(), key=lambda x: occurrences[x], reverse=True)
        result = bill_schema.dump(bills, many=True)
    else:
        response = Response(json.dumps(f"invalid model: {model}"), mimetype="application/json")
        response.error_code = 404
        return response
    return jsonify({"data": result})

def search_articles(terms):
    """
    Searches the articles model for the given terms.

    :terms: the given terms to search for
    :return: the articles with matching terms
    """
    occurrences = {}
    for term in terms:
        queries = []
        queries.append(Article.title.contains(term))
        queries.append(Article.description.contains(term))
        queries.append(Article.author.contains(term))
        queries.append(Article.language.contains(term))
        queries.append(Article.published.contains(term))
        queries.append(Article.category.contains(term))
        articles = Article.query.filter(or_(*queries))
        for article in articles:
            if not article in occurrences:
                occurrences[article] = 1
            else:
                occurrences[article] += 1
    return occurrences

def search_posts(terms):
    """
    Searches the posts model for the given terms.

    :terms: the given terms to search for
    :return: the posts with matching terms
    """
    occurrences = {}
    for term in terms:
        queries = []
        queries.append(Post.caption.contains(term))
        queries.append(Post.likes.contains(term))
        queries.append(Post.comments.contains(term))
        queries.append(Post.hashtags.contains(term))
        queries.append(Post.mentions.contains(term))
        queries.append(Post.tag.contains(term))
        posts = Post.query.filter(or_(*queries))
        for post in posts:
            if not post in occurrences:
                occurrences[post] = 1
            else:
                occurrences[post] += 1
    return occurrences

def search_bills(terms):
    """
    Searches the bills model for the given terms.

    :terms: the given terms to search for
    :return: the bills with matching terms
    """
    occurrences = {}
    for term in terms:
        queries = []
        queries.append(Bill.title.contains(term))
        queries.append(Bill.description.contains(term))
        queries.append(Bill.sponsor.contains(term))
        queries.append(Bill.introduced.contains(term))
        queries.append(Bill.sponsor_state.contains(term))
        queries.append(Bill.sponsor_party.contains(term))
        queries.append(Bill.summary.contains(term))
        queries.append(Bill.committee.contains(term))
        queries.append(Bill.subjects.contains(term))
        queries.append(Bill.status.contains(term))
        bills = Bill.query.filter(or_(*queries))
        for bill in bills:
            if not bill in occurrences:
                occurrences[bill] = 1
            else:
                occurrences[bill] += 1
    return occurrences

@app.route("/articles")
def get_articles_by_page():
    """
    Retrives articles in page sizes.

    :return: numResults number of articles per page specified, uses DEFAULT PAGE SIZE if 
    numResults is left empty
    """
    # Return results in size of pages
    page = request.args.get("page", type=int)
    num_results = request.args.get("numResults", type=int)
    title = request.args.get("title")
    author = request.args.get("author")
    language = request.args.get("language")
    published = request.args.get("published")
    category = request.args.get("category")
    sort = request.args.get("sort")
    asc = request.args.get("asc")

    # Query the database
    query = db.session.query(Article)

    # Filter
    if title is not None:
        query = query.filter(Article.title.contains(title))

    if author is not None:
        query = query.filter(Article.author.contains(author))

    if language is not None:
        query = query.filter(Article.language == language)

    if published is not None:
        query = query.filter(Article.published == published)

    if category is not None:
        query = query.filter(Article.category.contains(category))
        # else:
        #     query = query.filter(Article.category == category)

    # Sort
    if sort is not None and getattr(Article, sort) is not None:
        if asc is not None:
            query = query.order_by(getattr(Article, sort))
        else:
            query = query.order_by(desc(getattr(Article, sort)))

    count = query.count()
    if page:
        if not num_results:
            num_results = DEFAULT_PAGE_SIZE
        query = query.paginate(page=page, per_page=num_results, error_out=False).items
    result = article_schema.dump(query, many=True)
    return jsonify({"page":page, "count":count, "data":result, "numResults":num_results})

@app.route("/articles/id=<int:article_id>")
def get_article_by_id(article_id):
    """
    Retrives a single article with the matching id, if any.

    :article_id: the given id to look for
    :return: the article with the matching id, error 404 otherwise
    """
    a_query = db.session.query(Article)
    article = a_query.filter_by(id=article_id).first()
    if not article:
        response = Response(json.dumps({"error": f"{article_id} not found"}), mimetype="application/json")
        response.error_code = 404
        return response
    p_query = db.session.query(Post)
    post = p_query.filter_by(id=article.post_id).first()
    if not post:
        response = Response(json.dumps({"error": f"Post for article {article_id} not found"}), mimetype="application/json")
        response.error_code = 404
        return response
    
    result = article_schema.dump(article)
    post_result = post_schema.dump(post)
    
    return jsonify({"article": result, "post": post_result})

@app.route("/posts")
def get_post_by_page():
    """
    Retrives posts in page sizes.

    :return: numResults number of posts per page specified, uses DEFAULT PAGE SIZE if 
    numResults is left empty
    """
    # Return results in size of pages
    page = request.args.get("page", type=int)
    num_results = request.args.get("numResults", type=int)
    likes = request.args.get("likes")
    comments = request.args.get("comments")
    date = request.args.get("date")
    hashtags = request.args.get("hashtags")
    mentions = request.args.get("mentions")
    tag = request.args.get("tag")
    sort = request.args.get("sort")
    asc = request.args.get("asc")

    # Query the database
    query = db.session.query(Post)

    # Filter
    if likes is not None:
        query = query.filter(Post.likes == likes)

    if comments is not None:
        query = query.filter(Post.comments == comments)

    if date is not None:
        query = query.filter(Post.date == date)

    if hashtags is not None:
        query = query.filter(Post.hashtags.contains(hashtags))

    if mentions is not None:
        query = query.filter(Post.mentions.contains(mentions))
    
    if tag is not None:
        query = query.filter(Post.tag == tag)

    # Sort
    if sort is not None and getattr(Post, sort) is not None:
        if asc is not None:
            query = query.order_by(getattr(Post, sort))
        else:
            query = query.order_by(desc(getattr(Post, sort)))

    count = query.count()
    if page:
        if not num_results:
            num_results = DEFAULT_PAGE_SIZE
        query = query.paginate(page=page, per_page=num_results, error_out=False).items
    result = post_schema.dump(query, many=True)
    return jsonify({"page":page, "count":count, "data":result, "numResults":num_results})

@app.route("/posts/id=<int:post_id>")
def get_post_by_id(post_id):
    """
    Retrives a single post with the matching id, if any.
    
    :post_id: the given id to look for
    :return: the post with the matching id, error 404 otherwise
    """
    post = db.session.query(Post).filter_by(id=post_id).first()
    if not post:
        return Response(json.dumps({"error": f"{post_id} not found"}))
    
    bills = db.session.query(Bill).filter_by(post_id=post.id).all()
    articles = db.session.query(Article).filter_by(post_id=post.id).all()
    
    result = post_schema.dump(post)
    a_result = article_schema.dump(articles, many=True)
    b_result = bill_schema.dump(bills, many=True)

    return jsonify({"post":result, "articles":a_result, "bills":b_result})

@app.route("/bills")
def get_bill_by_page():
    """
    Retrives bills in page sizes.

    :return: numResults number of bills per page specified, uses DEFAULT PAGE SIZE if 
    numResults is left empty
    """
    # Return results in size of pages
    page = request.args.get("page", type=int)
    num_results = request.args.get("numResults", type=int)
    introduced = request.args.get("introduced")
    title = request.args.get("title")
    sponsor_state = request.args.get("sponsorState")
    sponsor_party = request.args.get("sponsorParty")
    status = request.args.get("status")
    num_cosponsors = request.args.get("numCosponsors")
    committee = request.args.get("committee")
    subjects = request.args.get("subjects")
    sort = request.args.get("sort")
    asc = request.args.get("asc")

    # Query the database
    query = db.session.query(Bill)

    # Filter
    if introduced is not None:
        query = query.filter(Bill.introduced == introduced)
    
    if title is not None:
        query = query.filter(Bill.title.contains(title))

    if sponsor_state is not None:
        query = query.filter(Bill.sponsor_state == sponsor_state)

    if sponsor_party is not None:
        query = query.filter(Bill.sponsor_party == sponsor_party)

    if status is not None:
        query = query.filter(Bill.status == status)

    if num_cosponsors is not None:
        query = query.filter(Bill.num_cosponsors == num_cosponsors)

    if committee is not None:
        query = query.filter(Bill.committee == committee)
    
    if subjects is not None:
        query = query.filter(Bill.subjects.contains(subjects))

    # Sort
    if sort is not None and getattr(Bill, sort) is not None:
        if asc is not None:
            query = query.order_by(getattr(Bill, sort))
        else:
            query = query.order_by(desc(getattr(Bill, sort)))

    count = query.count()
    if page:
        if not num_results:
            num_results = DEFAULT_PAGE_SIZE
        query = query.paginate(page=page, per_page=num_results, error_out=False).items
    result = bill_schema.dump(query, many=True)
    return jsonify({"page":page, "count":count, "data":result, "numResults":num_results})

@app.route("/bills/id=<int:bill_id>")
def get_bill_by_id(bill_id):
    """
    Retrives a single bill with the matching id, if any.

    :article_id: the given id to look for
    :return: the bill with the matching id, error 404 otherwise
    """
    bill = Bill.query.filter_by(id=bill_id).first()
    if not bill:
        response = Response(json.dumps({"error": f"{bill_id} not found"}))
        return response
    
    p_query = db.session.query(Post)
    post = p_query.filter_by(id=bill.post_id).first()
    if not post:
        response = Response(json.dumps({"error": f"Post for bill {bill_id} not found"}), mimetype="application/json")
        response.error_code = 404
        return response
    result = bill_schema.dump(bill)
    result_post = post_schema.dump(post)
    return jsonify({"bill": result, "post": result_post})

if __name__ == "__main__":
    """
    Runs app with the given environment variables
    """
    app.run(host="0.0.0.0", port=5050, debug=True)
