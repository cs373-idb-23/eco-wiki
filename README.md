# EcoWiki



## Group Info

**Group Name**: IDB 23
**Group Members**: *Aayush Gupta, Shaz Momin, Yundi Li, Imran Hussein, Eddie Cazares* 

#### Group Members (GitLab ID, EID):

- Aayush Gupta: mugloos, aog396
- Shaz Momin: shaz.momin, stm2422
- Yundi Li: rollingthundar, yl36429
- Imran Hussein: imranh02, ih5296
- Eddie Cazares: ecazares31, eac4364

#### Git SHA: 5542bc93165699e94288679ff275ae9c3dc469ab (to be updated once merge completes)
#### Project Leader: Aayush Gupta
**More information**: project leader is by alphabetical order,
the major responsibility is to assign work to team members 
and plan out meeting times
#### GitLab Pipelines: https://gitlab.com/cs373-idb-23/eco-wiki/-/pipelines
#### Website: https://www.ecowiki.me
#### Estimated completion time for each member:

**After Phase 4**

- Aayush Gupta: 30 hours
- Shaz Momin: 30 hours
- Yundi Li: 30 hours
- Imran Hussein: 30 hours
- Eddie Cazares: 30 hours

#### Actual completion time for each member:

**After Phase 4**

- Aayush Gupta: 35 hours
- Shaz Momin: 35 hours
- Yundi Li: 35 hours
- Imran Hussein: 35 hours
- Eddie Cazares: 35 hours

**Comments**:

## Project Info

**Project Name**: EcoWiki

**Project Info**: EcoWiki is a website where users can go to get up to date information on US policies/bills, current events, and trending Instagram posts related to the subject of environmental science. The site will display information related to both "for" and "againt" environmental related changes. This will help users find an aggregate of environment-related information. This project will mostly focus on solely US news, policies, and posts for now.


### APIs
- Gitlab: [GitLab API](https://docs.gitlab.com/ee/api/api_resources.html)
- Current Events: [Newsdata.io](https://newsdata.io/documentation)
- Community: [Instagram](https://developers.facebook.com/docs/instagram-api/)
- Policies: [ProPublica](https://projects.propublica.org/api-docs/congress-api/)
- Images: [Google Custom Seach](https://developers.google.com/custom-search/v1/overview)

- Postman Link: https://documenter.getpostman.com/view/25799511/2s93CExcGV

### Models

#### Current Events
- Instances: 600
- Attributes: title, description, author, language, category, publish date
- Media: News articles, images
- Connections: News is written on policies and how the community reacts.

#### Community
- Instances: 557
- Attributes: caption, likes, comments, publish date, hashtags, mentions, tag
- Media: Instagram post, photos, videos, comments
- Connections: Community reacts to policies and the news surrounding it.

#### Policies
- Instances: 865
- Attributes: bill id, title, description, date introduced, sponsor info, bill status, # of cosponsors, subjects, committee
- Media: pdf, amendment docs
- Connections: Policies are made in reaction to currently pressing issues of the society.

### Organizational Technique

One page per model with a grid-like view of cards presenting information based on the model

### Questions

1. What are the latest environmentally-friendly bills in U.S. Congress?
2. What kind of environmental posts are being shared in my country?
3. What is the state of the world in terms of our environment?
4. What is the latest news involving our environment?
5. What kind of changes is congress making that are effecting our environment?
